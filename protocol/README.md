# Protokoll #

Inhaltsverzeichnis

- [Vorlesungswoche 1](./woche1.md)
- [Vorlesungswoche 2](./woche2.md)
- [Vorlesungswoche 3](./woche3.md)
- [Vorlesungswoche 4](./woche4.md)
- [Vorlesungswoche 5](./woche5.md)
- [Vorlesungswoche 6](./woche6.md)
- [Vorlesungswoche 7](./woche7.md)
- [Vorlesungswoche 8](./woche8.md)
- [Vorlesungswoche 9](./woche9.md)
- [Vorlesungswoche 10](./woche10.md)
- [Vorlesungswoche 11](./woche11.md)
- [Vorlesungswoche 12](./woche12.md)
- [Vorlesungswoche 13](./woche13.md) ???
- [Vorlesungswoche 14](./woche14.md) ???

## Termine zur Veröffentlichung und Abgabe der Serien ##

| Serie | Ausgabe | Abgabe |
| :---- | :------ | :----- |
|     1 |  19.10. | 02.11. |
|     2 |  02.11. | 16.11. |
|     3 |  16.11. | 30.11. |
|     4 |  30.11. | 14.12. |
|     5 |  14.12. | 04.01. |
|     6 |  04.01. | 18.01. |
