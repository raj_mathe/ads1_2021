# Vorlesungswoche 7 (22.–28. November 2021) #

## Agenda ##

- [x] Orga: nochmalige Abstimmung der Studierenden über Präsenz v. Digital
  ---> überwiegende Mehrheit für weiteren Präsenzbetrieb.

Aspekte und Berechnungen mit binären Suchbäumen

- Aspekte, insbesondere *ausgeglichen*.
- Motivation: wieso wollen wir *ausgeglichene* Suchbäume?
  - Optimierung der Tiefe h vis-á-vis Anzahl der Knoten, n.
- Suchalgorithmus im Suchbaum
  - welche Verhältnisse müssen zw. den Knoten gelten?
- Ansatz bei INSERT und ROTATE, um *ausgeglichenen* Baum
  mit passenden Verhältnissen zw. Knoten zu erzeugen.

### TODOs (Studierende) ###

- weiter an Pflichtserie3 arbeiten und vor der Frist abgeben.
