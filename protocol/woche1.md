# Vorlesungswoche 1 (11.–17. Oktober 2021) #

## Agenda ##

- [ ] Orga:
    - [ ] Coronaregeln
    - [ ] Moodle - https://moodle2.uni-leipzig.de/course/view.php?id=34368
    - [ ] Leistungen:
        - 12 x freiwillig
        - 6 x Pflicht (-> 50% insges. nötig)
        - jede Übung ein weiterer in-Session Zettel
- [ ] Themen:
    - [ ] Komponenten eines Algorithmus (vllt zu basic)
    - [ ] Rekursion
    - [ ] Zeitkomplexität:
        - was kann man sagen, wenn man bspw. weiß T(n) ∈ O(n^2.1798)
        - (best?), worst, average case - wann passt was?
        - Good-to-knows:
            - g ∈ Ω(f) gdw. f ∈ O(g)
    - [ ] Ins & Outs vom Masterthm
        - T(n/b), wenn ¬(b | n)
        - ==> T(n), wenn n reellwertig?
        - Zeige: Rekursion gilt noch, unter Erweiterung
            - T(n) := T([n])
            - g(n) := g([n])
        solange b eine natürliche Zahl!!!
    - [ ] Anwendung - max (Halbierung) FFT

## Nächste Woche ##

-

### TODOs (Studierende) ###

-
