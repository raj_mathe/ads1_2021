# Vorlesungswoche 4 (1.–7. November 2021) #

## Agenda ##

- Gruppe 1:
  - Verkettete Listen
    - basic methods/attributes
  - Stacks + Queues
    - LIFO=FILO vs. FIFO=LILO
  -  PseudoCode-Algorithmus für NextGreaterElement mittels Stacks erarbeitet und diskutiert
    - ACHTUNG: im code-Ordner ([go-Variante](../code/golang/pkg/algorithms/stacks/next_greater_element/next_greater_element.go) und [python-Variante](../code/python/src/algorithms/stacks/next_greater_element.py)) habe ich einen vereinfachten Algorithmus implementiert.
- Gruppe 2:
  - PseudoCode-Algorithmus für NextGreaterElement mittels Stacks erarbeitet und diskutiert
    - ACHTUNG: im code-Ordner ([go-Variante](../code/golang/pkg/algorithms/stacks/next_greater_element/next_greater_element.go) und [python-Variante](../code/python/src/algorithms/stacks/next_greater_element.py)) habe ich einen vereinfachten Algorithmus implementiert.
  - Grundkonzepte für gerichtete/ungerichtete Graphen und Bäume besprochen

## Nächste Woche ##

- Sortierungsalgorithmen
- Bäume

### TODOs (Studierende) ###

- VL-Inhalte aus Wochen 3 + 4 durchgehen
  - jeden Sortierungsalgorithmus und MaxHeap verstehen
  - Darstellung von Daten als Bäume verstehen
- freiwillige ÜB 4 + Pflichtserie 2
