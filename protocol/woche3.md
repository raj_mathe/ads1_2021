# Vorlesungswoche 3 (25.–31. Oktober 2021) #

## Agenda ##

- Gruppe 1:
    - Suchalgorithmen
    - Pseudocode von seq. Suche, Binärsuche, Interpolationssuche
- Gruppe 2:
    - Erinnerung an / Motivation durch Classes + Pointers
    - Verkettete Listen
      - basic methods/attributes
    - Stacks + Queues
      - LIFO=FILO vs. FIFO=LILO

## Nächste Woche ##

- ab verketteten Listen

### TODOs (Studierende) ###

- VL-Inhalte aus Wochen 1–3 durchgehen
- freiwillige ÜB 1-3
