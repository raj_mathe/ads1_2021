<style>
    table tbody tr td {
        vertical-align: top;
    }
    table tbody tr td:nth-child(1) {
        font-size: 10pt;
    }
    table tbody tr td:nth-child(2), table tbody tr td:nth-child(3) {
        font-size: 8pt;
    }
</style>

# Glossar #

| Symbol | Referenz             | Kurze Beschreibung |
| :----- | :------------------- | :----------------- |
