#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# IMPORTS
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

from src.core.log import *;
from src.core.config import *;
from src.setup.display import *;
from src.setup import assets;
from src.algorithms.exports import *;

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# ENDPOINT run interactive modus
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

def runInteractive():
    '''
    Startet Programm im interaktiven Modus (Konsole).
    '''
    logPlain(assets.Logo());
    logWarn('Interaktiver Modus noch nicht implementiert.');
    return;

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# ENDPOINT run non-interactive
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

def runNonInteractive(path: str):
    '''
    Liest Fälle aus Configdatei aus und führt Algorithmen darauf aus.
    '''
    config = ReadConfigFile(path);
    cases = GetAttribute(config, 'parts', 'cases', expectedtype=list, default=[]);
    logPlain(assets.Logo());
    for caseindex, case in enumerate(cases):
        command = GetAttribute(case, 'command', expectedtype=str, default='');
        descr = GetAttribute(case, 'description', expectedtype=str, default='');
        inputs = GetAttribute(case, 'inputs', expectedtype=dict, default={});

        DisplayStartOfCase(caseindex, descr);

        try:
            if command == 'algorithm-search-sequential':
                SequentialSearch(L=inputs['L'], x=inputs['x']);
            elif command == 'algorithm-search-binary':
                BinarySearch(L=inputs['L'], x=inputs['x']);
            elif command == 'algorithm-search-interpolation':
                InterpolationSearch(L=inputs['L'], x=inputs['x'], u=0, v=len(inputs['L'])-1);
            elif command == 'algorithm-search-jump':
                JumpSearchLinear(L=inputs['L'], x=inputs['x'], m=inputs['m']);
            elif command == 'algorithm-search-jump-exp':
                JumpSearchExponentiell(L=inputs['L'], x=inputs['x']);
            elif command == 'algorithm-search-ith-element':
                FindIthSmallest(L=inputs['L'], i=inputs['i']);
            elif command == 'algorithm-search-ith-element-dc':
                FindIthSmallestDC(L=inputs['L'], i=inputs['i']);
            elif command == 'algorithm-search-poison':
                FindPoison(L=inputs['L']);
            elif command == 'algorithm-search-poison-fast':
                FindPoisonFast(L=inputs['L']);
            elif command == 'algorithm-stacks-next-greater-element':
                NextGreaterElement(L=inputs['L']);
            elif command == 'algorithm-sum-maxsub':
                MaxSubSum(L=inputs['L']);
            elif command == 'algorithm-sum-maxsub-dc':
                MaxSubSumDC(L=inputs['L']);
            else:
                raise ValueError('Command \033[1m{}\033[0m nicht erkannt'.format(command));
        except Exception as e:
            logError(e);
    DisplayEndOfCase();
    return;
