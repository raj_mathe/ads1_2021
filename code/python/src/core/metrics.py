# !/usr/bin/env python3
# -*- coding: utf-8 -*-

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# IMPORTS
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

from __future__ import annotations;
from datetime import datetime;
from datetime import timedelta;

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# GLOBAL VARIABLES
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

_ctr_time = None;
_ctr_space = None;
_tmr = None;

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# CLASS counter
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

class Counter(object):
    value: int;

    def __init__(self):
        self.reset();

    def __str__(self) -> str:
        return str(self.value);

    def add(self, n: int = 1):
        self.value += n;
        return self;

    def reset(self):
        self.value = 0;
        return self;

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# CLASS timer
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

class Timer(object):
    _time_elapsed: timedelta;
    _time_current: datetime;
    _running: bool

    def __init__(self):
        self.reset();

    def __str__(self) -> str:
        return str(self._time_elapsed);

    @property
    def elapsedTime(self) -> timedelta:
        return self._time_elapsed;

    def start(self):
        self._time_current = datetime.now();
        self._running = True
        return self;

    def stop(self):
        if self._running:
            t0 = self._time_current;
            t1 = datetime.now();
            delta = t1 -  t0;
            self._time_current = t1;
            self._time_elapsed += delta;
        self._running = False
        return self;

    def reset(self):
        t = datetime.now();
        delta = t - t;
        self._time_current = t;
        self._time_elapsed = delta;
        self._running = False
        return self;

## Initialisierung:
_ctr_time = Counter();
_ctr_moves = Counter();
_ctr_space = Counter();
_tmr = Timer();

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# METHODS
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

def ResetMetrics():
    global _ctr_time;
    global _ctr_moves;
    global _ctr_space;
    global _tmr;

    _ctr_time.reset();
    _ctr_moves.reset();
    _ctr_space.reset();
    _tmr.reset();
    return;

def StartMetrics():
    _tmr.start()
    return;

def StopMetrics():
    _tmr.stop()
    return;

def AddTimeCost(n: int = 1):
    global _ctr_time;
    _ctr_time.add(n);
    return;

def AddMovesCost(n: int = 1):
    global _ctr_moves;
    _ctr_moves.add(n);
    return;

def AddSpaceCost(n: int = 1):
    global _ctr_space;
    _ctr_space.add(n);
    return;

def SetSpaceCost(n: int):
    global _ctr_space;
    _ctr_space.value = n;
    return;

def GetTimeCost() -> int:
    return _ctr_time.value;

def GetMovesCost() -> int:
    return _ctr_moves.value;

def GetSpaceCost() -> int:
    return _ctr_space.value;

def GetTimeElapsed() -> timedelta:
    global _tmr;
    _tmr.stop();
    return _tmr.elapsedTime;
