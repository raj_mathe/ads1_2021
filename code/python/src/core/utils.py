#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# IMPORTS
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

import re;
from textwrap import dedent;

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# METHODS ansi
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

def StripAnsi(text: str) -> str:
    return re.sub(r'\x1b[^m]*m', '', text);

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# METHODS strings
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

def DedentIgnoreFirstLast(text: str) -> str:
    text = '\n'.join(re.split(r'\n', text)[1:][:-1])
    return dedent(text);

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# METHODS strings -> bool
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

def IsTrue(text: str) -> bool:
    text = text.strip();
    if re.match(r'^(true|t|yes|y|1|\+|\+1)$', text, re.IGNORECASE):
        return True;
    return False;

def IsFalse(text: str) -> bool:
    text = text.strip();
    if re.match(r'^(false|f|no|n|0|-|-1)$', text, re.IGNORECASE):
        return True;
    return False;
