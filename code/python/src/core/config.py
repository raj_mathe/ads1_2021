#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# IMPORTS
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

from src.local.typing import *;
from src.local.config import *;

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# GLOBAL VARIABLES
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

#

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# METHOD read config file
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

def ReadConfigFile(path: str) -> dict:
    with open(path, 'r') as fp:
        spec = load(fp, Loader=FullLoader);
        assert isinstance(spec, dict), 'Die Configdatei muss eines Dictionary-Typs sein';
    return spec;

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# METHOD extract attribut from dictionary/list
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

def GetAttribute(
    obj: Any,
    *keys: Union[str, int, List[Union[str, int]]],
    expectedtype: Union[Type, Tuple[Type]] = Any,
    default: Any = None
) -> Any:
    if len(keys) == 0:
        return obj;
    nextkey = keys[0];
    nextkey = nextkey if isinstance(nextkey, list) else [ nextkey ];
    try:
        for key in nextkey:
            if isinstance(key, str) and isinstance(obj, dict) and key in obj:
                value = obj[key];
                if len(keys) <= 1:
                    return value if isinstance(value, expectedtype) else default;
                else:
                    return GetAttribute(obj[key], *keys[1:], expectedtype=expectedtype, default=default);
            elif isinstance(key, int) and isinstance(obj, (list,tuple)) and key < len(obj):
                value = obj[key];
                if len(keys) <= 1:
                    return value if isinstance(value, expectedtype) else default;
                else:
                    return GetAttribute(obj[key], *keys[1:], expectedtype=expectedtype, default=default);
    except:
        pass;
    if len(keys) <= 1:
        return default;
    path = ' -> '.join([ str(key) for key in keys ]);
    raise Exception('Konnte \033[1m{}\033[0m im Objekt nicht finden!'.format(path));
