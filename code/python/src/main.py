#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# IMPORTS
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

import os;
import sys;

_path_to_python_project = os.path.abspath(os.path.join(os.path.abspath(__file__), '..', '..'));
sys.path.insert(0, _path_to_python_project);
os.chdir(_path_to_python_project);

from src.core.log import *;
from src.setup.cli import *;
from src.setup import appconfig;
from src.endpoints.exports import *;

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# GLOBAL VARIABLES/CONSTANTS
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

PATH_TO_CONFIG: str = '../config.yml';

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# MAIN PROCESS
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

def enter(
    mode: Union[str, None],
    it: bool,
    quiet: bool,
    debug: bool,
    checks: bool,
    colour: bool,
    config: Union[str, None],
    **_
):
    # Programmeinstellungen initialisieren
    if not (mode == 'run' and it):
        SetQuietMode(quiet);
        SetDebugMode(debug);
    SetAnsiMode(colour);
    appconfig.SetAppConfigPerformChecks(checks);

    config = config if isinstance(config, str) else PATH_TO_CONFIG;

    # Wenn der Artefakt ohne Argument aufgerufen wird, keinen Fehler melden, sondern im it-Modus ausführen
    if mode is None:
        endpoint_runInteractive();
        return;
    # Sonst Commands behandeln
    if mode == 'help':
        endpoint_help();
        return;
    elif mode == 'version':
        endpoint_version();
        return;
    elif mode == 'run':
        if it:
            endpoint_runInteractive();
        else:
            endpoint_runNonInteractive(path=config);
        return;
    else:
        endpoint_runInteractive();
        return;
    return;

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# EXECUTE CODE
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

if __name__ == '__main__':
    sys.tracebacklimit = 0;
    try:
        args = GetArgumentsFromCli(*sys.argv[1:]);
    except Exception as e:
        endpoint_help();
        exit(1);
    enter(
        mode=args.mode,
        it=args.it,
        quiet=args.quiet,
        debug=args.debug,
        checks=args.checks,
        colour=args.colour,
        config=args.config,
    );
