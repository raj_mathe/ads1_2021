#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# IMPORTS
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

from src.local.io import *;

from src.core.utils import DedentIgnoreFirstLast
from src.setup.cli import GetArgumentParser;

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# METHODS assets
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

def Help() -> str:
    parser = GetArgumentParser();
    with io.StringIO() as fp:
        parser.print_help(fp)
        text = fp.getvalue();
    return text;

def Logo() -> str:
    ## NOTE: expandiert ansi nicht:
    # with open('assets/LOGO', 'r') as fp:
    #     logo = (''.join(fp.readlines())).strip();
    logo = DedentIgnoreFirstLast('''
    +--------------------+
    |      \033[32;1mAlgoDat I\033[0m     |
    +--------------------+
    ''') + '\n';
    return logo;

def Version() -> str:
    with open('assets/VERSION', 'r') as fp:
        version = (''.join(fp.readlines())).strip();
    return version;
