#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# IMPORTS
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

from src.local.typing import *;

from src.core.log import *;
from src.core.metrics import *;
from src.setup.cli import GetArgumentParser;

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# METHODS display help
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

def DisplayHelpMessage():
    parser = GetArgumentParser();
    parser.print_help();
    return;

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# METHODS display case
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

def DisplayStartOfCase(index: int, descr: Any):
    DisplayBar(80);
    if not isinstance(descr, str) or descr == '':
        logPlain('\033[92;1mCASE {index}\033[0m.'.format(index=index));
    else:
        logPlain('\033[92;1mCASE {index}\033[0m (\033[1;2m{descr}\033[0m).'.format(index=index, descr=descr));
    return;

def DisplayEndOfCase():
    DisplayBar(80);
    return;

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# METHODS display value
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

def RepresentValue(value: Any) -> Any:
    if value is None or isinstance(value, (str, bool, int, float)):
        return value;
    elif isinstance(value, list):
        if len(value) > 10:
            value = value[:3] + [ '...' ] + value[-2:];
        # return '[{}, ..., {}]'.format(', '.join(value[:3]), ', '.join(value[-2:])
        return value;
    return value;

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# METHODS display algorithm start/end
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

def DisplayStartOfAlgorithm(name: str, *_: Any, **inputs: Any):
    logPlain('Ausführung vom Algorithmus: \033[92;1m{}\033[0m'.format(name));
    logPlain('INPUTS');
    for varname, value in inputs.items():
        logPlain('  - {} = {}'.format(varname, RepresentValue(value)))
    return;

def DisplayEndOfAlgorithm(*_: Any, **outputs: Any):
    logPlain('OUTPUTS:')
    for varname, value in outputs.items():
        logPlain('  - {} = {}'.format(varname, RepresentValue(value)))
    return;

def DisplayMetrics():
    logPlain('Dauer der Ausführung:   t  = \033[1m{}\033[0m'.format(GetTimeElapsed()));
    logPlain('Kosten (Zeit):        T(n) = \033[1m{}\033[0m'.format(displayCost(GetTimeCost())));
    logPlain('Kosten (#Züge):       M(n) = \033[1m{}\033[0m'.format(displayCost(GetMovesCost())));
    logPlain('Kosten (Platz):       S(n) = \033[1m{}\033[0m'.format(displayCost(GetSpaceCost())));
    return;

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# METHODS Verschiedenes
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

def DisplayBar(n: int = 80):
    logPlain('+{}+'.format('-'*n));
    return;

def displayCost(cost: int) -> str:
    return str(cost) if cost > 0 else '-';
