#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# IMPORTS
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

from argparse import ArgumentError
from src.local.typing import *;

from src.core.log import *;
from src.core.utils import DedentIgnoreFirstLast;
from src.core.utils import IsTrue;
from src.core.utils import IsFalse;

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# GLOBAL VARIABLES
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

parser = None;

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# METHODS
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

def GetArgumentParser() -> argparse.ArgumentParser:
    global parser;
    if not isinstance(parser, argparse.ArgumentParser):
        parser = argparse.ArgumentParser(
            prog='code/main.py',
            description=DedentIgnoreFirstLast('''
            \033[93;1mBeschreibung:\033[0m
            \033[93;2mEin Programm zur Ausführung verschiedener Algorithmen aus dem Kurs AlgoDat I.\033[0m
            '''),
            formatter_class=argparse.RawTextHelpFormatter,
        );
        parser.add_argument('mode',
            nargs='?',
            choices=['version', 'help', 'run'],
            help=DedentIgnoreFirstLast('''
            help    = Hilfsanleitung anzeigen.
            version = Version anzeigen.
            run     = Algorithmen ausführen.
            '''),
        );
        parser.add_argument('--it',          action='store_true',          help='Startet das Programm im interaktiven Modus.')
        parser.add_argument('-q', '--quiet', action='store_true',          help='Blendet alle Konsole-Messages aus.')
        parser.add_argument('--debug',       action='store_true',          help='Blendet Debugging-Befehle ein.')
        parser.add_argument('--checks', nargs=1, type=str, default=['False'],
            help=DedentIgnoreFirstLast('''
            (bool)   Ob vor und nach Ausführung von Algorithmen Checks
                     auf Inputs/Outputs ausgeführt werden sollen.
            '''),
        ),
        parser.add_argument('--colour', nargs=1, type=str, default=['False'], help='(bool)   Ob Logging färblich angezeigt wird.')
        parser.add_argument('--config', nargs=1, type=str,                    help='(string) Pfad zur Configdatei (nur für run Endpunkt).');
    return parser;

def GetArgumentsFromCli(*cli_args: str) -> argparse.Namespace:
    parser = GetArgumentParser();
    args = parser.parse_args(cli_args);
    args.checks=IsTrue(args.checks[0]);
    args.colour=IsTrue(args.colour[0]);
    return args;
