#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# IMPORTS
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

from src.local.io import *;
from src.local.misc import *;
from src.local.system import *;
from src.local.typing import *;

from src.core.config import *;

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# GLOBAL VARIABLES
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

_config: Dict[str, bool] = dict(
    display = True,
    metrics = True,
    checks = False,
);

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# METHOD getters
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

def AppConfigInitialise():
    '''
    Extrahiert Inhalte einer YAML Config und parset dies als Struktur
    '''
    global _config;
    config = ReadConfigFile('assets/config.yml');
    _config['display'] = GetAttribute(config, 'options', 'display', expectedtype=bool, default=True);
    _config['metrics'] = GetAttribute(config, 'options', 'metrics', expectedtype=bool, default=True);
    _config['checks'] = GetAttribute(config, 'options', 'checks', expectedtype=bool, default=True);
    return;

def AppConfigFancyMode() -> bool:
    return _config['display'];

def AppConfigShowMetrics() -> bool:
    return _config['metrics'];

def AppConfigPerformChecks() -> bool:
    return _config['checks'];

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# METHOD setters
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

def SetAppConfigFancyMode(mode: bool):
    global _config;
    _config['display'] = mode;
    return;

def SetAppConfigShowMetrics(mode: bool):
    global _config;
    _config['metrics'] = mode;
    return;

def SetAppConfigPerformChecks(mode: bool):
    global _config;
    _config['checks'] = mode;
    return;
