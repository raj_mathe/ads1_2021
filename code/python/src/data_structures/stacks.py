#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# IMPORTS
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

from local.typing import *;

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# GLOBAL VARIABLES/CONSTANTS
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

#

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# CLASS Stack
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

class Stack(object):
    _initialised: bool;
    values: List[int];

    def __init__(self):
        self._initialised = False;

    def INIT(self):
        self.values = [];
        self._initialised = True;

    def EMPTY(self) -> bool:
        return not self._initialised or len(self.values) == 0;

    def PUSH(self, x: int):
        if not self._initialised:
            raise Exception('Stack not initialised!');
        self.values.append(x);

    def TOP(self) -> int:
        if self.EMPTY():
            raise Exception('Cannot read from empty stack!');
        return self.values[-1];

    def POP(self) -> int:
        if self.EMPTY():
            raise Exception('Cannot remove from empty stack!');
        self.values = self.values[:-1];

    def __str__(self) -> str:
        if len(self.values) == 0:
            return '-';
        return ', '.join([str(x) for x in self.values]);
