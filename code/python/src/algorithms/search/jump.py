#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# IMPORTS
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

from src.local.maths import *;
from src.local.typing import *;

from src.core.log import *;
from src.algorithms.search.sequential import SequentialSearch;
from src.algorithms.methods import *;

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# GLOBAL VARIABLES/CONSTANTS
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

#

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# CHECKS
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

def preChecks(L: List[int], **_):
    assert L == sorted(L), 'Ungültiger Input: L muss aufsteigend sortiert sein!';
    ## NOTE: nicht prüfen, ob Duplikate existieren. Das ist nur eine erwünschte aber keine notwendige Annahme.
    return;

def postChecks(L: List[int], x: int, index: int, **_):
    if x in L:
        assert index >= 0, 'Der Algorithmus sollte nicht -1 zurückgeben.';
        assert L[index] == x, 'Der Algorithmus hat den falschen Index bestimmt.';
    else:
        assert index == -1, 'Der Algorithmus sollte -1 zurückgeben.';
    return;

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# ALGORITHM jump search
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

@algorithmInfos(name='Sprungsuche', outputnames=['index'], preChecks=preChecks, postChecks=postChecks)
def JumpSearchLinear(L: List[int], x: int, m: int) -> int:
    '''
    Inputs: L = Liste von Zahlen, x = Zahl, m = lineare Sprunggröße.

    Annahmen:
    - L sei aufsteigend sortiert.
    - Idealerweise: L enthält keine Duplikate.
    - Idealerweise: Abstände zw. Elementen nicht uniform.

    Outputs: Position von x in L, sonst −1 wenn x nicht in L.
    '''
    i = 0;
    while i*m < len(L):
        AddTimeCost();
        offset = i*m;
        block = L[offset:][:m];
        elementAfterBlock = block[-1] + 1;
        if x < elementAfterBlock:
            logDebug('Element muss sich im Block [{i0}, {i1}) befinden.'.format(i0 = i*m, i1 = (i+1)*m));
            index = SequentialSearch(L=block, x=x);
            if index >= 0:
                index += offset; # NOTE: muss wegen Offset kompensieren
            return index;
        logDebug('Element befindet sich nicht im im Block [{i0}, {i1}) befinden.'.format(i0 = i*m, i1 = (i+1)*m));
        i += 1;
    return -1;

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# ALGORITHM jump search - exponentiell
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

@algorithmInfos(name='Sprungsuche (exponentiell)', outputnames=['index'], preChecks=preChecks, postChecks=postChecks)
def JumpSearchExponentiell(L: List[int], x: int) -> int:
    '''
    Inputs: L = Liste von Zahlen, x = Zahl.

    Annahmen:
    - L sei aufsteigend sortiert.
    - Idealerweise: L enthält keine Duplikate.
    - Idealerweise: Abstände zw. Elementen nicht uniform.

    Outputs: Position von x in L, sonst −1 wenn x nicht in L.
    '''
    i0 = 0;
    i1 = 1;
    while i0 < len(L):
        AddTimeCost();
        block = L[i0:i1];
        elementAfterBlock = block[-1] + 1;
        if x < elementAfterBlock:
            logDebug('Element muss sich im Block [{i0}, {i1}) befinden.'.format(i0 = i0, i1 = i1));
            index = SequentialSearch(L=block, x=x);
            if index >= 0:
                index += i0; # NOTE: muss wegen Offset kompensieren
            return index;
        logDebug('Element befindet sich nicht im Block [{i0}, {i1}) befinden.'.format(i0 = i0, i1 = i1));
        i0 = i1;
        i1 *= 2;
    return -1;
