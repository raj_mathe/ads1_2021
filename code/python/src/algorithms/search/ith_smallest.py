#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# IMPORTS
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

from src.local.maths import *;
from src.local.typing import *;

from src.core.log import *;
from src.algorithms.search.sequential import SequentialSearch;
from src.algorithms.methods import *;

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# GLOBAL VARIABLES/CONSTANTS
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

#

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# CHECKS
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

def preChecks(L: List[int], i: int, **_):
    assert 1 <= i and i <= len(L), 'Der Wert von i muss zw. {lb} und {ub} liegen.'.format(lb = 1, ub = len(L));
    assert sorted(L) == sorted(list(set(L))), 'Ungültiger Input: L darf keine Duplikate enthalten!';
    return;

def postChecks(L: List[int], i: int, value: int, **_):
    L = sorted(L);
    assert L[i-1] == value, 'Der Algorithmus hat versagt.';
    return;

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# ALGORITHM find ith smallest element
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

@algorithmInfos(name='Auswahlproblem (i. kleinstes Element)', outputnames=['value'], preChecks=preChecks, postChecks=postChecks)
def FindIthSmallest(L: List[int], i: int) -> int:
    '''
    Inputs: L = Liste von Zahlen, i = Ordinalzahl

    Annahmen:

    - L enthält keine Duplikate.
    - L enthält mindestens i Elemente.

    Outputs: Wert des i. kleinste Element in L.
             Beachte 1.kleinstes <==> Minimum.
    '''
    index = 0;
    minValue = L[0];
    AddTimeCost(len(L));
    for i_ in range(1, len(L)):
        if L[i_] < minValue:
            index = i_;
            minValue = L[i_];
    if i == 1:
        logDebug('Das i. kleinste Element wurde gefunden.');
        return minValue;
    else:
        logDebug('Entfernte Minimum: {value}.'.format(value = minValue));
        i = i - 1;
        return FindIthSmallest(L=L[:index] + L[(index+1):], i=i);

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# ALGORITHM find ith smallest element (D & C)
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

@algorithmInfos(name='Auswahlproblem (i. kleinstes Element, D & C)', outputnames=['value'], preChecks=preChecks, postChecks=postChecks)
def FindIthSmallestDC(L: List[int], i: int) -> int:
    '''
    Inputs: L = Liste von Zahlen, i = Ordinalzahl

    Annahmen:

    - L enthält keine Duplikate.
    - L enthält mindestens i Elemente.

    Outputs: Wert des i. kleinste Element in L.
             Beachte 1.kleinstes <==> Minimum.
    '''
    AddTimeCost();
    p = L[len(L)-1]; # NOTE: Pivotelement kann beliebig gewählt werden
    Ll = [ x for x in L if x < p ];
    Lr = [ x for x in L if x > p ];
    if len(Ll) == i - 1:
        logDebug('Es gibt   i-1 Elemente vor p={p}. ==> i. kleinste Element = p'.format(p=p));
        return p;
    elif len(Ll) >= i:
        logDebug('Es gibt  >= i Elemente vor p={p}. ==> Suche in linker Hälfte!'.format(p=p));
        return FindIthSmallestDC(L=Ll, i=i);
    else:
        i = i - (len(Ll) + 1)
        logDebug('Es gibt < i-1 Elemente vor p={p}. ==> Suche in rechter Hälfte!'.format(p=p));
        return FindIthSmallestDC(L=Lr, i=i);
