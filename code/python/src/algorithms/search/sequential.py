#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# IMPORTS
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

from src.local.maths import *;
from src.local.typing import *;

from src.core.log import *;
from src.algorithms.methods import *;

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# GLOBAL VARIABLES/CONSTANTS
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

#

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# CHECKS
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

def preChecks(L: List[int], **_):
    # Keine Checks!
    return;

def postChecks(L: List[int], x: int, index: int, **_):
    if x in L:
        assert index >= 0, 'Der Algorithmus sollte nicht -1 zurückgeben.';
        assert L[index] == x, 'Der Algorithmus hat den falschen Index bestimmt.';
    else:
        assert index == -1, 'Der Algorithmus sollte -1 zurückgeben.';
    return;

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# ALGORITHM sequential search
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

@algorithmInfos(name='Sequenziellsuchalgorithmus', outputnames=['index'], preChecks=preChecks, postChecks=postChecks)
def SequentialSearch(L: List[int], x: int) -> int:
    '''
    Inputs: L = Liste von Zahlen, x = Zahl.
    Outputs: Position von x in L, sonst −1 wenn x nicht in L.
    '''
    n = len(L);
    for i in range(n):
        AddTimeCost();
        if L[i] == x:
            logDebug('Element in Position {} gefunden.'.format(i));
            return i;
        logDebug('Element nicht in Position {}.'.format(i));
    return -1;
