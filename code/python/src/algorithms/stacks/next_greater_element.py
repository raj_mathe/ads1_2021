#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# IMPORTS
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

from src.local.typing import *;

from src.core.log import *;
from src.core.metrics import *;
from src.data_structures.stacks import Stack;
from src.algorithms.methods import *;

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# GLOBAL VARIABLES/CONSTANTS
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

#

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# CHECKS
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

def preChecks(L: List[int], **_):
    # TODO
    return;

def postChecks(L: List[int], **_):
    # TODO
    return;

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# ALGORITHM next greater element
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

@algorithmInfos(name='NextGreaterElement (with stacks)', outputnames=['pairs'], preChecks=preChecks, postChecks=postChecks)
def NextGreaterElement(L: List[int]) -> List[Tuple[int,int]]:
    '''
    Inputs: L = Liste von Zahlen.
    Outputs: Liste von Paaren von Elementen und ihrem nächsten größeren Element
    '''
    output = [];

    S = Stack();
    S.INIT();

    for i in range(len(L)):
        logDebug('Stack S | {S}'.format(S=S));
        logDebug('Nächstes List-Element L[{i}] = {el} betrachten'.format(i=i, el=L[i]));
        nextElement = L[i];

        logDebug('Alle top Elemente vom Stack, die < nextElement sind, mit L[i] paaren');
        # Führe aus, bis top Element >= nextElement oder Stack leer ist.
        logDebug('Stack S | {S}'.format(S=S));
        while not S.EMPTY():
            element = S.TOP();
            # falls element < next Element, zum Output hinzufügen und vom Stack entfernen
            if element < nextElement:
                logDebug('Stack S | {S}; top Element < nextElement; ==> pop und Paar zum Output hinzufügen'.format(S=S))
                output.append((element, nextElement));
                S.POP();
                AddMovesCost();
                logDebug('Stack S | {S}'.format(S=S));
            # falls top Element > next Element, auf Stack lassen und Schleife abbrechen.
            elif element > nextElement:
                break
            # falls top Element == next Element, vom Stack entfernen und Schleife abbrechen.
            else:
                S.POP()
                AddMovesCost()
                break
        logDebug('Stack S | {S}'.format(S=S));

        logDebug('L[{i}] auf Stack legen'.format(i=i));
        S.PUSH(nextElement);
        AddMovesCost();

    logDebug('Stack S | {S}'.format(S=S));

    # was übrig bleibt hat kein größeres Element
    logDebug('Alles übrige auf Stack hat kein nächstes größeres Element')
    while not S.EMPTY():
        logDebug('Stack S | {S}'.format(S=S));
        element = S.TOP();
        S.POP();
        AddMovesCost();
        output.append((element, -1));

    logDebug('Stack S | {S}'.format(S=S))
    return output;
