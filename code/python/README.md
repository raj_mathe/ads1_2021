# ADS - Python-Projekt #

**Python** ist eine sehr verbreitete Sprache, die eine Riesencommunity hat.
Sie ist sehr populär unter Wissenschaftlern und Ingenieuren.
Ein Vorteil mit Python ist, dass man sehr leicht und schnell Prototypen entwickeln kann.

Dieses Projekt ist erneut kein nötiger Bestandteil des Kurses,
sondern nur für Wissbegierige gedacht.
Zunächst bietet sich an, sich die Algorithmen im Unterordner [`src/algorithms`](./src/algorithms) anzuschauen,
z.&nbsp;B. [`src/algorithms/search/binary.py`](./src/algorithms/search/binary.py) für den Binärsuchalgorithmus,
ohne irgendetwas installieren zu müssen.

**HINWEIS 1:** _Bei meiner Implementierung kann es zu leichten Abweichungen kommen. Bitte **stets** an dem Material im VL-Skript sich orientieren. Der Hauptzweck der Code-Projekte besteht darin, dass Wissbegierige die Algorithmen konkret ausprobieren können. Alle theoretischen Aspekte werden jedoch im Skript und in den Übungen ausführlicher erklärt._

Den Code kann man auch durch die u.&nbsp;s. Anweisungen selber austesten.

**HINWEIS 2:** _Während hier die Anweisungen ausführlich sind und klappen sollten,
bitte nur auf eigener Gewähr diesen Code benutzen._

### Systemvoraussetzungen ###

- Python version 3.x.x (idealerweise zumindest 3.9.5)

## Einrichten mittels **Makefile** ##

Führe jeweils

```bash
make setup
make run
# oder für interaktiven Modus
make run-it
```

aus, um Packages zu installieren
bzw. den Code auszuführen.
Siehe [`Makefile`](./Makefile) für Details zu den Vorgängen.

Wer Makefile benutzt kann die u.&nbsp;s. Anweisungen ignorieren.

## Setup ##

Requirements (Packages) einmalig mittels

```bash
python3 -m pip install -r requirements; # linux, osx
py -3   -m pip install -r requirements; # Windows
```

installieren lassen.

## Ausführung ##

Zum Anzeigen der Hilfsanleitung in einer Konsole folgenden Befehl ausführen

```bash
python3 src/main.py help; # linux, OSX
py -3   src/main.py help; # Windows
```

Zur Ausführung der Algorithmen auf Fälle in der [Config-Datei](./../config.yml)
in einer Konsole folgenden Befehl ausführen

```bash
python3 src/main.py run; # linux, OSX
py -3   src/main.py run; # Windows
```

Mit dem `--debug` Flag werden Infos über Schritte mit angezeigt.
</br>
Mit dem `--colour true/false` Argument wird der Farbenmodus ein-/ausgeschaltet.
</br>
Mit dem `--config path/to/config` Argument kann man andere Config-Dateien verwenden.

## Entwicklung ##

Gerne kann man den Code benutzen, in einem eigenen Repository weiter entwickeln,
und mit den im Kurs präsentierten Algorithmen und Fällen herumexperimentieren.
