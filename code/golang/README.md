# ADS - Golang-Projekt #

**Golang** ist eine relativ moderne Programmiersprache, die immer populärer wird und viele Vorteile anbietet.

Dieses Projekt ist erneut kein nötiger Bestandteil des Kurses,
sondern nur für Wissbegierige gedacht.
Zunächst bietet sich an, sich die Algorithmen im Unterordner [`pkg/algorithms`](./pkg/algorithms) anzuschauen,
z.&nbsp;B. [`pkg/algorithms/search/binary/binary.go`](./pkg/algorithms/search/binary/binary.go) für den Binärsuchalgorithmus,
ohne irgendetwas installieren zu müssen.

**HINWEIS 1:** _Bei meiner Implementierung kann es zu leichten Abweichungen kommen. Bitte **stets** an dem Material im VL-Skript sich orientieren. Der Hauptzweck der Code-Projekte besteht darin, dass Wissbegierige die Algorithmen konkret ausprobieren können. Alle theoretischen Aspekte werden jedoch im Skript und in den Übungen ausführlicher erklärt._

Den Code kann man auch durch die u.&nbsp;s. Anweisungen selber austesten.

**HINWEIS 2:** _Während hier die Anweisungen ausführlich sind und klappen sollten,
bitte nur auf eigener Gewähr diesen Code benutzen._

## Systemvoraussetzungen ##

- **Bash**. Dies kommt mit OSX (Terminal) und Linux. Für Windows-User braucht man [git-for-windows](https://gitforwindows.org) zu installieren, was auch bash mit installiert.
- [**go**](https://golang.org/dl/) Version **1.17.x**. (Man kann bestimmt bei späteren Releases höhere Versionen benutzen. Man muss lediglich dann in [`go.mod`](./go.mod) die Version hochstellen.)

Alle u.&nbsp;s. Befehle sollen in einer Bash-Konsole von diesem Ordner aus ausgeführt werden.

## Einrichten mittels **Makefile** ##

Führe jeweils

```bash
make setup
make build
make run
# oder für interaktiven Modus
make run-it
```

aus, um Packages zu installieren
bzw. den Code zu kompilieren
bzw. den Code auszuführen.
Siehe [`Makefile`](./Makefile) für Details zu den Vorgängen.

Wer Makefile benutzt kann die u.&nbsp;s. Anweisungen ignorieren.

## Setup/Kompilieren ##


1. Requirements (Packages) einmalig mittels

    ```bash
    go get $( cat requirements )
    ```

    installieren. Oder man führe den Befehl
    für jede Zeile aus [`requirements`](./requirements) aus,
    </br>
    z.&nbsp;B. `go get github.com/akamensky/argparse@v1.3.1`
    installiert eines der Packages.

2. Codetree mittels

    ```bash
    go build -o ads main.go
    ```

    kompilieren.
    </br>
    Statt `-o ads` kann man einen beliebigen Pfad verwenden.
    </br>
    In Unix kann man `-o path/to/folder/ads` verwenden.
    </br>
    In Windows kann man `-o path/to/folder/ads.exe` verwenden.

## Ausführung ##

Nach Kompilieren wird ein Artefakt namens `ads` gebaut,
den man per Doppelklick ausführen kann.
Alternativ kann man in der Konsole im Ordner mit dem Artefakt einfach

```bash
ads
```

ausführen.

## Entwicklung ##

Gerne kann man den Code benutzen, in einem eigenen Repository weiter entwickeln,
und mit den im Kurs präsentierten Algorithmen und Fällen herumexperimentieren.
