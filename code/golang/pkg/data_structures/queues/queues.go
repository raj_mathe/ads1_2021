package queues

/* ---------------------------------------------------------------- *
 * IMPORTS
 * ---------------------------------------------------------------- */

import (
	"fmt"
	"strings"
)

/* ---------------------------------------------------------------- *
 * GLOBAL VARIABLES/CONSTANTS
 * ---------------------------------------------------------------- */

//

/* ---------------------------------------------------------------- *
 * TYPE
 * ---------------------------------------------------------------- */

type QueueInt struct {
	values *[]int
}

/* ---------------------------------------------------------------- *
 * METHODS queues
 * ---------------------------------------------------------------- */

func CREATE() QueueInt {
	return QueueInt{}
}

func (S *QueueInt) INIT() {
	S.values = &[]int{}
}

func (S QueueInt) Length() int {
	if S.values == nil {
		return 0
	}
	return len(*S.values)
}

func (S QueueInt) String() string {
	if S.EMPTY() {
		return "-"
	}
	values := []string{}
	n := S.Length()
	for i := n - 1; i >= 0; i-- {
		value := (*S.values)[i]
		values = append(values, fmt.Sprintf("%v", value))
	}
	return fmt.Sprintf(strings.Join(values, " -> "))
}

func (S QueueInt) EMPTY() bool {
	return S.Length() == 0
}

func (S *QueueInt) ENQUEUE(x int) {
	if S.values == nil {
		panic("Queue not initialised!")
	}
	*S.values = append(*S.values, x)
}

func (S *QueueInt) FRONT() int {
	if S.EMPTY() {
		panic("Cannot read from empty queue!")
	}
	return (*S.values)[0]
}

func (S *QueueInt) DEQUEUE() {
	if S.EMPTY() {
		panic("Cannot remove from empty queue!")
	}
	*S.values = (*S.values)[1:]
}
