package sequential

/* ---------------------------------------------------------------- *
 * IMPORTS
 * ---------------------------------------------------------------- */

import (
	"ads/internal/core/logging"
	"ads/internal/core/metrics"
)

/* ---------------------------------------------------------------- *
 * GLOBAL VARIABLES/CONSTANTS
 * ---------------------------------------------------------------- */

//

/* ---------------------------------------------------------------- *
 * ALGORITHM sequential search
 * ---------------------------------------------------------------- */

/*
Inputs: L = Liste von Zahlen, x = Zahl.
Outputs: Position von x in L, sonst −1 wenn x nicht in L.
*/
func SequentialSearch(L []int, x int) int {
	n := len(L)
	for i := 0; i < n; i++ {
		metrics.AddTimeCost()
		if L[i] == x {
			logging.Debug("Element in Position %[1]v gefunden.", i)
			return i
		}
		logging.Debug("Element nicht in Position %[1]v.", i)
	}
	return -1
}
