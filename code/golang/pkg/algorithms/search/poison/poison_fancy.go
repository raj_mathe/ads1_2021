package poison

/* ---------------------------------------------------------------- *
 * IMPORTS
 * ---------------------------------------------------------------- */

import (
	"fmt"

	"ads/internal/core/metrics"
	"ads/internal/core/utils"
	"ads/internal/setup"
)

/* ---------------------------------------------------------------- *
 * GLOBAL VARIABLES/CONSTANTS
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */

//

/* ---------------------------------------------------------------- *
 * CHECKS
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */

func preChecks(L []int, _ ...interface{}) error {
	s := utils.SumListInt(L)
	if !(s > 0) {
		return fmt.Errorf("Mindestens ein Getränk muss vergiftet sein!")
	} else if !(s <= 1) {
		return fmt.Errorf("Höchstens ein Getränk darf vergiftet sein!")
	}
	return nil
}

func postChecks(L []int, index int, _ ...interface{}) error {
	if !(index >= 0) {
		return fmt.Errorf("Der Algorithmus hat kein vergiftetes Getränk gefunden, obwohl per Annahme eines existiert.")
	} else if !(L[index] > 0) {
		return fmt.Errorf("Der Algorithmus hat das vergiftete Getränk nicht erfolgreich bestimmt.")
	}
	return nil
}

/* ---------------------------------------------------------------- *
 * ALGORITHM find poison + Display
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */

func FancyFindPoison(input_L []int) (int, error) {
	var name = "Giftsuche (O(n) Vorkoster)"
	var inputs = map[string]interface{}{
		"L": input_L,
	}
	var outputs map[string]interface{}
	var (
		output_index int
	)
	var err error

	do_once := true
	for do_once {
		do_once = false

		// Start Message
		setup.DisplayStartOfAlgorithm(name, inputs)

		// Prechecks:
		if setup.AppConfigPerformChecks() {
			err = preChecks(input_L)
			if err != nil {
				break
			}
		}

		// Ausführung des Algorithmus:
		metrics.ResetMetrics()
		metrics.StartMetrics()
		output_index = FindPoison(input_L)
		metrics.StopMetrics()
		outputs = map[string]interface{}{
			"index": output_index,
		}

		// Metriken anzeigen
		if setup.AppConfigShowMetrics() {
			setup.DisplayMetrics()
		}

		// End Message
		setup.DisplayEndOfAlgorithm(outputs)

		// Postchecks:
		if setup.AppConfigPerformChecks() {
			err = postChecks(input_L, output_index)
			if err != nil {
				break
			}
		}
	}

	return output_index, err
}

/* ---------------------------------------------------------------- *
 * ALGORITHM find poison fast + Display
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */

func FancyFindPoisonFast(input_L []int) (int, error) {
	var name = "Giftsuche (O(log(n)) Vorkoster)"
	var inputs = map[string]interface{}{
		"L": input_L,
	}
	var outputs map[string]interface{}
	var (
		output_index int
	)
	var err error

	do_once := true
	for do_once {
		do_once = false

		// Start Message
		setup.DisplayStartOfAlgorithm(name, inputs)

		// Prechecks:
		if setup.AppConfigPerformChecks() {
			err = preChecks(input_L)
			if err != nil {
				break
			}
		}

		// Ausführung des Algorithmus:
		metrics.ResetMetrics()
		metrics.StartMetrics()
		output_index = FindPoisonFast(input_L)
		metrics.StopMetrics()
		outputs = map[string]interface{}{
			"index": output_index,
		}

		// Metriken anzeigen
		if setup.AppConfigShowMetrics() {
			setup.DisplayMetrics()
		}

		// End Message
		setup.DisplayEndOfAlgorithm(outputs)

		// Postchecks:
		if setup.AppConfigPerformChecks() {
			err = postChecks(input_L, output_index)
			if err != nil {
				break
			}
		}
	}

	return output_index, err
}
