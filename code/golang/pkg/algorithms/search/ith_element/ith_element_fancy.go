package ith_element

/* ---------------------------------------------------------------- *
 * IMPORTS
 * ---------------------------------------------------------------- */

import (
	"fmt"
	"sort"

	"ads/internal/core/metrics"
	"ads/internal/core/utils"
	"ads/internal/setup"
)

/* ---------------------------------------------------------------- *
 * GLOBAL VARIABLES/CONSTANTS
 * ---------------------------------------------------------------- */

//

/* ---------------------------------------------------------------- *
 * CHECKS
 * ---------------------------------------------------------------- */

func preChecks(L []int, i int, _ ...interface{}) error {
	if !(1 <= i && i <= len(L)) {
		return fmt.Errorf("Der Wert von i muss zw. %[1]v und %[2]v liegen.", 1, len(L))
	} else if !utils.ContainsNoDuplicatesListInt(L) {
		return fmt.Errorf("Ungültiger Input: L darf keine Duplikate enthalten!")
	}
	return nil
}

func postChecks(L []int, i int, value int, _ ...interface{}) error {
	L_ := make([]int, len(L))
	copy(L_, L)
	sort.Ints(L_)
	if !(L_[i-1] == value) {
		return fmt.Errorf("Das i=%[1]v. kleinste Element ist %[2]v und nicht %[3]v.", i, L_[i-1], value)
	}
	return nil
}

/* ---------------------------------------------------------------- *
 * ALGORITHM find ith smallest + Display
 * ---------------------------------------------------------------- */

func FancyFindIthSmallest(input_L []int, input_i int) (int, error) {
	var name = "Auswahlproblem (i. kleinstes Element)"
	var inputs = map[string]interface{}{
		"L": input_L,
		"i": input_i,
	}
	var outputs map[string]interface{}
	var (
		output_value int
	)
	var err error

	do_once := true
	for do_once {
		do_once = false

		// Start Message
		setup.DisplayStartOfAlgorithm(name, inputs)

		// Prechecks:
		if setup.AppConfigPerformChecks() {
			err = preChecks(input_L, input_i)
			if err != nil {
				break
			}
		}

		// Ausführung des Algorithmus:
		metrics.ResetMetrics()
		metrics.StartMetrics()
		output_value = FindIthSmallest(input_L, input_i)
		metrics.StopMetrics()
		outputs = map[string]interface{}{
			"value": output_value,
		}

		// Metriken anzeigen
		if setup.AppConfigShowMetrics() {
			setup.DisplayMetrics()
		}

		// End Message
		setup.DisplayEndOfAlgorithm(outputs)

		// Postchecks:
		if setup.AppConfigPerformChecks() {
			err = postChecks(input_L, input_i, output_value)
			if err != nil {
				break
			}
		}
	}

	return output_value, err
}

/* ---------------------------------------------------------------- *
 * ALGORITHM  find ith smallest (D & C) + Display
 * ---------------------------------------------------------------- */

func FancyFindIthSmallestDC(input_L []int, input_i int) (int, error) {
	var name = "Auswahlproblem (i. kleinstes Element, D & C)"
	var inputs = map[string]interface{}{
		"L": input_L,
		"i": input_i,
	}
	var outputs map[string]interface{}
	var (
		output_value int
	)
	var err error

	do_once := true
	for do_once {
		do_once = false

		// Start Message
		setup.DisplayStartOfAlgorithm(name, inputs)

		// Prechecks:
		if setup.AppConfigPerformChecks() {
			err = preChecks(input_L, input_i)
			if err != nil {
				break
			}
		}

		// Ausführung des Algorithmus:
		metrics.ResetMetrics()
		metrics.StartMetrics()
		output_value = FindIthSmallestDC(input_L, input_i)
		metrics.StopMetrics()
		outputs = map[string]interface{}{
			"value": output_value,
		}

		// Metriken anzeigen
		if setup.AppConfigShowMetrics() {
			setup.DisplayMetrics()
		}

		// End Message
		setup.DisplayEndOfAlgorithm(outputs)

		// Postchecks:
		if setup.AppConfigPerformChecks() {
			err = postChecks(input_L, input_i, output_value)
			if err != nil {
				break
			}
		}
	}

	return output_value, err
}
