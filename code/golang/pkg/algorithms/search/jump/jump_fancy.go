package jump

/* ---------------------------------------------------------------- *
 * IMPORTS
 * ---------------------------------------------------------------- */

import (
	"fmt"

	"ads/internal/core/metrics"
	"ads/internal/core/utils"
	"ads/internal/setup"
)

/* ---------------------------------------------------------------- *
 * GLOBAL VARIABLES/CONSTANTS
 * ---------------------------------------------------------------- */

//

/* ---------------------------------------------------------------- *
 * CHECKS
 * ---------------------------------------------------------------- */

func preChecks(L []int, _ ...interface{}) error {
	if !utils.IsSortedListInt(L) {
		return fmt.Errorf("Ungültiger Input: L muss aufsteigend sortiert sein!")
	}
	// NOTE: nicht prüfen, ob Duplikate existieren. Das ist nur eine erwünschte aber keine notwendige Annahme.
	return nil
}

func postChecks(L []int, x int, index int, _ ...interface{}) error {
	if utils.ArrayContains(L, x) {
		if !(index >= 0) {
			return fmt.Errorf("Der Algorithmus sollte nicht -1 zurückgeben.")
		}
		if L[index] != x {
			return fmt.Errorf("Der Algorithmus hat den falschen Index bestimmt.")
		}
	} else {
		if index != -1 {
			return fmt.Errorf("Der Algorithmus sollte -1 zurückgeben.")
		}
	}
	return nil
}

/* ---------------------------------------------------------------- *
 * ALGORITHM jump search + Display
 * ---------------------------------------------------------------- */

func FancyJumpSearchLinear(input_L []int, input_x int, input_m int) (int, error) {
	var name = "Sprungsuche"
	var inputs = map[string]interface{}{
		"L": input_L,
		"x": input_x,
		"m": input_m,
	}
	var outputs map[string]interface{}
	var (
		output_index int
	)
	var err error

	do_once := true
	for do_once {
		do_once = false

		// Start Message
		setup.DisplayStartOfAlgorithm(name, inputs)

		// Prechecks:
		if setup.AppConfigPerformChecks() {
			err = preChecks(input_L, input_x, input_m)
			if err != nil {
				break
			}
		}

		// Ausführung des Algorithmus:
		metrics.ResetMetrics()
		metrics.StartMetrics()
		output_index = JumpSearchLinear(input_L, input_x, input_m)
		metrics.StopMetrics()
		outputs = map[string]interface{}{
			"index": output_index,
		}

		// Metriken anzeigen
		if setup.AppConfigShowMetrics() {
			setup.DisplayMetrics()
		}

		// End Message
		setup.DisplayEndOfAlgorithm(outputs)

		// Postchecks:
		if setup.AppConfigPerformChecks() {
			err = postChecks(input_L, input_x, output_index)
			if err != nil {
				break
			}
		}
	}

	return output_index, err
}

/* ---------------------------------------------------------------- *
 * ALGORITHM jump search - exponentiell + Display
 * ---------------------------------------------------------------- */

func FancyJumpSearchExponentiell(input_L []int, input_x int) (int, error) {
	var name = "Sprungsuche (exponentiell)"
	var inputs = map[string]interface{}{
		"L": input_L,
		"x": input_x,
	}
	var outputs map[string]interface{}
	var (
		output_index int
	)
	var err error

	do_once := true
	for do_once {
		do_once = false

		// Start Message
		setup.DisplayStartOfAlgorithm(name, inputs)

		// Prechecks:
		if setup.AppConfigPerformChecks() {
			err = preChecks(input_L, input_x)
			if err != nil {
				break
			}
		}

		// Ausführung des Algorithmus:
		metrics.ResetMetrics()
		metrics.StartMetrics()
		output_index = JumpSearchExponentiell(input_L, input_x)
		metrics.StopMetrics()
		outputs = map[string]interface{}{
			"index": output_index,
		}

		// Metriken anzeigen
		if setup.AppConfigShowMetrics() {
			setup.DisplayMetrics()
		}

		// End Message
		setup.DisplayEndOfAlgorithm(outputs)

		// Postchecks:
		if setup.AppConfigPerformChecks() {
			err = postChecks(input_L, input_x, output_index)
			if err != nil {
				break
			}
		}
	}

	return output_index, err
}
