package maxsubsum

/* ---------------------------------------------------------------- *
 * IMPORTS
 * ---------------------------------------------------------------- */

import (
	"fmt"

	"ads/internal/core/metrics"
	"ads/internal/setup"
)

/* ---------------------------------------------------------------- *
 * GLOBAL VARIABLES/CONSTANTS
 * ---------------------------------------------------------------- */

//

/* ---------------------------------------------------------------- *
 * CHECKS
 * ---------------------------------------------------------------- */

func preChecks(L []int, _ ...interface{}) error {
	if !(len(L) > 0) {
		return fmt.Errorf("Liste darf nicht leer sein.")
	}
	return nil
}

func postChecks(L []int, _ ...interface{}) error {
	// TODO
	return nil
}

/* ---------------------------------------------------------------- *
 * METHOD Algorithm + Display
 * ---------------------------------------------------------------- */

func FancyMaxSubSum(input_L []int) (int, int, int, error) {
	var name = "MaxSubSum (Maximale Teilsumme)"
	var inputs = map[string]interface{}{
		"L": input_L,
	}
	var outputs map[string]interface{}
	var (
		output_maxSum    int
		output_indexFrom int
		output_indexTo   int
	)
	var err error

	do_once := true
	for do_once {
		do_once = false

		// Start Message
		setup.DisplayStartOfAlgorithm(name, inputs)

		// Prechecks:
		if setup.AppConfigPerformChecks() {
			err = preChecks(input_L)
			if err != nil {
				break
			}
		}

		// Ausführung des Algorithmus:
		metrics.ResetMetrics()
		metrics.StartMetrics()
		output_maxSum, output_indexFrom, output_indexTo = MaxSubSum(input_L)
		metrics.StopMetrics()
		outputs = map[string]interface{}{
			"maxSum":     output_maxSum,
			"index_from": output_indexFrom,
			"index_to":   output_indexTo,
		}

		// Metriken anzeigen
		if setup.AppConfigShowMetrics() {
			setup.DisplayMetrics()
		}

		// End Message
		setup.DisplayEndOfAlgorithm(outputs)

		// Postchecks:
		if setup.AppConfigPerformChecks() {
			err = postChecks(input_L, output_maxSum, output_indexFrom, output_indexTo)
			if err != nil {
				break
			}
		}
	}

	return output_maxSum, output_indexFrom, output_indexTo, err
}

/* ---------------------------------------------------------------- *
 * ALGORITHM max sub sum (D & C)
 * ---------------------------------------------------------------- */

func FancyMaxSubSumDC(input_L []int) (int, int, int, error) {
	var name = "MaxSubSum (Maximale Teilsumme mit D & C)"
	var inputs = map[string]interface{}{
		"L": input_L,
	}
	var outputs map[string]interface{}
	var (
		output_maxSum    int
		output_indexFrom int
		output_indexTo   int
	)
	var err error

	do_once := true
	for do_once {
		do_once = false

		// Start Message
		setup.DisplayStartOfAlgorithm(name, inputs)

		// Prechecks:
		if setup.AppConfigPerformChecks() {
			err = preChecks(input_L)
			if err != nil {
				break
			}
		}

		// Ausführung des Algorithmus:
		metrics.ResetMetrics()
		metrics.StartMetrics()
		output_maxSum, output_indexFrom, output_indexTo = MaxSubSumDC(input_L)
		metrics.StopMetrics()
		outputs = map[string]interface{}{
			"maxSum":     output_maxSum,
			"index_from": output_indexFrom,
			"index_to":   output_indexTo,
		}

		// Metriken anzeigen
		if setup.AppConfigShowMetrics() {
			setup.DisplayMetrics()
		}

		// End Message
		setup.DisplayEndOfAlgorithm(outputs)

		// Postchecks:
		if setup.AppConfigPerformChecks() {
			err = postChecks(input_L, output_maxSum, output_indexFrom, output_indexTo)
			if err != nil {
				break
			}
		}
	}

	return output_maxSum, output_indexFrom, output_indexTo, err
}
