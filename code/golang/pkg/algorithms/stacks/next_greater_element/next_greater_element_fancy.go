package next_greater_element

/* ---------------------------------------------------------------- *
 * IMPORTS
 * ---------------------------------------------------------------- */

import (
	"ads/internal/core/metrics"
	"ads/internal/setup"
)

/* ---------------------------------------------------------------- *
 * GLOBAL VARIABLES/CONSTANTS
 * ---------------------------------------------------------------- */

//

/* ---------------------------------------------------------------- *
 * CHECKS
 * ---------------------------------------------------------------- */

func preChecks(L []int, _ ...interface{}) error {
	// TODO
	return nil
}

func postChecks(L []int, pairs [][2]int, _ ...interface{}) error {
	// TODO
	return nil
}

/* ---------------------------------------------------------------- *
 * ALGORITHM binary search + Display
 * ---------------------------------------------------------------- */

func FancyNextGreaterElement(input_L []int) ([][2]int, error) {
	var name = "Next Greater Element"
	var inputs = map[string]interface{}{
		"L": input_L,
	}
	var outputs map[string]interface{}
	var (
		pairs [][2]int
	)
	var err error

	do_once := true
	for do_once {
		do_once = false

		// Start Message
		setup.DisplayStartOfAlgorithm(name, inputs)

		// Prechecks:
		if setup.AppConfigPerformChecks() {
			err = preChecks(input_L)
			if err != nil {
				break
			}
		}

		// Ausführung des Algorithmus:
		metrics.ResetMetrics()
		metrics.StartMetrics()
		pairs = NextGreaterElement(input_L)
		metrics.StopMetrics()
		outputs = map[string]interface{}{
			"pairs": pairs,
		}

		// Metriken anzeigen
		if setup.AppConfigShowMetrics() {
			setup.DisplayMetrics()
		}

		// End Message
		setup.DisplayEndOfAlgorithm(outputs)

		// Postchecks:
		if setup.AppConfigPerformChecks() {
			err = postChecks(input_L, pairs)
			if err != nil {
				break
			}
		}
	}

	return pairs, err
}
