package next_greater_element

/* ---------------------------------------------------------------- *
 * IMPORTS
 * ---------------------------------------------------------------- */

import (
	"ads/internal/core/logging"
	"ads/internal/core/metrics"
	"ads/pkg/data_structures/stacks"
)

/* ---------------------------------------------------------------- *
 * GLOBAL VARIABLES/CONSTANTS
 * ---------------------------------------------------------------- */

//

/* ---------------------------------------------------------------- *
 * ALGORITHM next greater element
 * ---------------------------------------------------------------- */

/*
Inputs: L = Liste von Zahlen.

Outputs: Liste von Paaren von Elementen und ihrem nächsten größeren Element
*/
func NextGreaterElement(L []int) [][2]int {
	output := [][2]int{}

	S := stacks.CREATE()
	S.INIT()

	for i := 0; i < len(L); i++ {
		logging.Debug("Stack S | %v", S)
		logging.Debug("Nächstes List-Element L[%v] = %v betrachten", i, L[i])
		nextElement := L[i]

		logging.Debug("Alle top Elemente vom Stack, die < nextElement sind, mit L[i] paaren")
		// Führe aus, bis top Element >= nextElement oder Stack leer ist.
		for !S.EMPTY() { // ACHTUNG: schreibe 'while' im Pseudocode, denn dies ist eine while-Schleife in golang
			element := S.TOP()
			if element < nextElement {
				// falls top Element < next Element, zum Output hinzufügen und vom Stack entfernen
				logging.Debug("Stack S | %v; top Element < nextElement; ==> pop und Paar zum Output hinzufügen", S)
				output = append(output, [2]int{element, nextElement})
				S.POP()
				metrics.AddMovesCost()
				logging.Debug("Stack S | %v", S)
			} else if element > nextElement {
				// falls top Element > next Element, auf Stack lassen und Schleife abbrechen.
				break
			} else {
				// falls top Element == next Element, vom Stack entfernen und Schleife abbrechen.
				S.POP()
				metrics.AddMovesCost()
				break
			}
		}

		logging.Debug("L[%v] auf Stack legen", i)
		S.PUSH(nextElement)
		metrics.AddMovesCost()
	}

	logging.Debug("Stack S | %v", S)

	// was übrig bleibt hat kein größeres Element
	logging.Debug("Alles übrige auf Stack hat kein nächstes größeres Element")
	for !S.EMPTY() { // ACHTUNG: schreibe 'while' im Pseudocode, denn dies ist eine while-Schleife in golang
		logging.Debug("Stack S | %v", S)
		element := S.TOP()
		output = append(output, [2]int{element, -1})
		S.POP()
		metrics.AddMovesCost()
	}

	logging.Debug("Stack S | %v", S)

	return output
}
