module ads

go 1.17

require (
	github.com/akamensky/argparse v1.3.1
	github.com/davecgh/go-spew v1.1.1
	github.com/lithammer/dedent v1.1.0
	github.com/pmezard/go-difflib v1.0.0
	github.com/stretchr/objx v0.3.0
	github.com/stretchr/testify v1.7.0
	gopkg.in/yaml.v3 v3.0.0-20210107192922-496545a6307b
)
