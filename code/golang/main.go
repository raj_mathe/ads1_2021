package main

/* ---------------------------------------------------------------- *
 * IMPORTS
 * ---------------------------------------------------------------- */

import (
	"embed"
	"os"

	"ads/internal/core/logging"
	"ads/internal/setup"
	"ads/internal/setup/cli"
	"ads/internal/types"

	endpoints_print "ads/internal/endpoints/print"
	endpoints_run "ads/internal/endpoints/run"
)

/* ---------------------------------------------------------------- *
 * GLOBAL VARIABLES
 * ---------------------------------------------------------------- */

var (
	// !!! NOTE: do not remove the following "comment", as it is a preprocessing instruction !!!
	//go:embed assets/*
	res    embed.FS
	assets = map[string]string{
		"appconfig": "assets/config.yml",
		"version":   "assets/VERSION",
		"logo":      "assets/LOGO",
		"help":      "assets/HELP",
	}
)

/* ---------------------------------------------------------------- *
 * METHOD main
 * ---------------------------------------------------------------- */

func main() {
	var arguments *types.CliArguments
	var (
		err1       error
		err2       error
		err3       error
		cmdMissing bool
		showChecks bool
	)

	// assets festlegen
	setup.Res = res
	setup.Assets = assets

	// cli arguments parsen
	arguments, err1 = cli.ParseCli(os.Args)
	cmdMissing = cli.ParseCliCommandMissing(err1)

	// Programmeinstellungen initialisieren
	showChecks = false
	if err1 == nil {
		if !(arguments.ModeRun.Happened() && arguments.InteractiveMode()) {
			logging.SetQuietMode(arguments.QuietModeOn())
			logging.SetDebugMode(arguments.DebugModeOn())
		}
		logging.SetAnsiMode(arguments.ShowColour())
		showChecks = arguments.ShowChecks()
	}
	err2 = setup.AppConfigInitialise()
	setup.SetAppConfigPerformChecks(showChecks)

	// Fehler melden (fatal)
	if err1 != nil && !cmdMissing {
		logging.Fatal(err1)
	}
	if err2 != nil {
		logging.Fatal(err2)
	}
	// Wenn der Artefakt ohne Argument aufgerufen wird, keinen Fehler melden, sondern im it-Modus ausführen
	if cmdMissing {
		endpoints_run.RunInteractive()
		return
	}
	// Sonst Commands behandeln
	if arguments.ModeHelp.Happened() {
		endpoints_print.Help()
		return
	} else if arguments.ModeVersion.Happened() {
		endpoints_print.Version()
		return
	} else if arguments.ModeRun.Happened() {
		if arguments.InteractiveMode() {
			endpoints_run.RunInteractive()
		} else {
			err3 = endpoints_run.RunNonInteractive(arguments.GetConfigFile())
			if err3 != nil {
				logging.Fatal(err3)
			}
		}
		return
	} else {
		endpoints_run.RunInteractive()
	}
}
