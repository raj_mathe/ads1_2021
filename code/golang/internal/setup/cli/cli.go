package cli

/* ---------------------------------------------------------------- *
 * IMPORTS
 * ---------------------------------------------------------------- */

import (
	"fmt"

	"github.com/akamensky/argparse"

	"ads/internal/types"
	"ads/pkg/re"
)

/* ---------------------------------------------------------------- *
 * GLOBAL VARIABLES
 * ---------------------------------------------------------------- */

var Parser *argparse.Parser

/* ---------------------------------------------------------------- *
 * LOCAL VARIABLES / CONSTANTS
 * ---------------------------------------------------------------- */

var optionsQuiet = argparse.Options{
	Help:     "Blendet alle Konsole-Messages aus.",
	Required: false,
	Default:  false,
}

var optionsChecks = argparse.Options{
	Help:     "Ob vor und nach Ausführung von Algorithmen Checks auf Inputs/Outputs ausgeführt werden sollen.",
	Required: false,
	// NOTE: no `Boolean` option available!
	Default: "false",
}

var optionsDebug = argparse.Options{
	Help:     "Blendet Debugging-Befehle ein.",
	Required: false,
	Default:  false,
}

var optionsInteractive = argparse.Options{
	Help:     "Startet das Programm im interaktiven Modus.",
	Required: false,
	Default:  false,
}

var optionsColour = argparse.Options{
	Help:     "Ob Logging färblich angezeigt wird.",
	Required: false,
	// NOTE: no `Boolean` option available!
	Default: "false",
}

var optionsConfigFile = argparse.Options{
	Help:     "Pfad zur Configdatei (nur für run Endpunkt).",
	Required: false,
	Default:  "code/config.yml",
}

/* ---------------------------------------------------------------- *
 * METHODS parse cli
 * ---------------------------------------------------------------- */

// Parst cli flags.
func ParseCli(args []string) (*types.CliArguments, error) {
	var err error
	Parser = argparse.NewParser(
		"cli parser",
		"\033[93;2mEin Programm zur Ausführung verschiedener Algorithmen aus dem Kurs AlgoDat I.\033[0m",
	)
	arguments := types.CliArguments{
		ModeHelp:    Parser.NewCommand("help", "Hilfsanleitung anzeigen"),
		ModeVersion: Parser.NewCommand("version", "Version anzeigen."),
		ModeRun:     Parser.NewCommand("run", "Algorithmen ausführen."),
		Quiet:       Parser.Flag("q", "quiet", &optionsQuiet),
		Debug:       Parser.Flag("", "debug", &optionsDebug),
		Interactive: Parser.Flag("", "it", &optionsInteractive),
		Checks:      Parser.String("", "checks", &optionsChecks),
		Colour:      Parser.String("", "colour", &optionsColour),
		ConfigFile:  Parser.String("", "config", &optionsConfigFile),
	}
	err = Parser.Parse(args)
	return &arguments, err
}

// Prüft, ob der Parser nur deshalb fehlschlägt, weil ein Command fehlt.
func ParseCliCommandMissing(err error) bool {
	// FIXME: unschöne Lösung. Leider ist Error-Typ im Package versteckt
	return re.Matches(`(?i)(command required)`, fmt.Sprintf("%v", err))
}
