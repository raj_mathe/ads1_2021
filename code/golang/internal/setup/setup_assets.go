package setup

/* ---------------------------------------------------------------- *
 * IMPORTS
 * ---------------------------------------------------------------- */

import (
	"embed"
	"fmt"
	"log"
	"strings"

	"ads/internal/core/utils"
)

/* ---------------------------------------------------------------- *
 * GLOBAL VARIABLES
 * ---------------------------------------------------------------- */

var Res embed.FS
var Assets map[string]string

/* ---------------------------------------------------------------- *
 * METHOD read assets
 * ---------------------------------------------------------------- */

func ReadAsset(key string) string {
	var found bool
	if _, found = Assets[key]; !found {
		log.Fatal(fmt.Sprintf("Key \033[1m%s\033[0m not found in dictionary!", key))
	}
	data, err := Res.ReadFile(Assets[key])
	if err != nil {
		log.Fatal(err)
	}
	text := string(data)
	return text
}

/* ---------------------------------------------------------------- *
 * METHODS assets
 * ---------------------------------------------------------------- */

func Help() string {
	contents := ReadAsset("help")
	return utils.DedentAndExpand(contents)
}

func Logo() string {
	contents := ReadAsset("logo")
	return utils.DedentAndExpand(contents)
}

func Version() string {
	return strings.Trim(ReadAsset("version"), "\n")
}

func AppConfigYaml() string {
	return strings.Trim(ReadAsset("appconfig"), "\n")
}
