package setup

/* ---------------------------------------------------------------- *
 * IMPORTS
 * ---------------------------------------------------------------- */

import (
	"io/ioutil"

	"gopkg.in/yaml.v3"

	"ads/internal/types"
)

/* ---------------------------------------------------------------- *
 * METHODS
 * ---------------------------------------------------------------- */

// Erstellt eine Struktur, die die Infos aus der Config-Datei erfasst
func NewUserConfig() types.UserConfig {
	return types.UserConfig{}
}

// Extrahiert Inhalte einer YAML Config und parset dies als Struktur
func GetUserConfig(path string, config *types.UserConfig) error {
	contents, err := ioutil.ReadFile(path)
	if err == nil {
		err = yaml.Unmarshal(contents, config)
	}
	return err
}
