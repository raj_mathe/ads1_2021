package setup

/* ---------------------------------------------------------------- *
 * IMPORTS
 * ---------------------------------------------------------------- */

import (
	"gopkg.in/yaml.v3"

	"ads/internal/types"
)

/* ---------------------------------------------------------------- *
 * GLOBAL CONSTANTS
 * ---------------------------------------------------------------- */

var AppConfig = types.AppConfig{}

/* ---------------------------------------------------------------- *
 * METHODS getters
 * ---------------------------------------------------------------- */

// Extrahiert Inhalte einer YAML Config und parset dies als Struktur
func AppConfigInitialise() error {
	contents := AppConfigYaml()
	err := yaml.Unmarshal([]byte(contents), &AppConfig)
	return err
}

func AppConfigFancyMode() bool {
	return AppConfig.Options.Display
}

func AppConfigShowMetrics() bool {
	return AppConfig.Options.Metrics
}

func AppConfigPerformChecks() bool {
	return AppConfig.Options.Checks
}

/* ---------------------------------------------------------------- *
 * METHODS setters
 * ---------------------------------------------------------------- */

func SetAppConfigFancyMode(value bool) {
	AppConfig.Options.Display = value
}

func SetAppConfigShowMetrics(value bool) {
	AppConfig.Options.Metrics = value
}

func SetAppConfigPerformChecks(value bool) {
	AppConfig.Options.Checks = value
}
