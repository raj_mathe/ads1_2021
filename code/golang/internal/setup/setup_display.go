package setup

/* ---------------------------------------------------------------- *
 * IMPORTS
 * ---------------------------------------------------------------- */

import (
	"fmt"
	"reflect"
	"strings"

	"ads/internal/core/logging"
	"ads/internal/core/metrics"
)

/* ---------------------------------------------------------------- *
 * METHOD display case start/end
 * ---------------------------------------------------------------- */

func DisplayStartOfCase(index int, descr *string) {
	DisplayBar(80)
	if descr == nil || *descr == "" {
		logging.Plain("\033[92;1mCASE %[1]v\033[0m.", index)
	} else {
		logging.Plain("\033[92;1mCASE %[1]v\033[0m (\033[1;2m%[2]s\033[0m).", index, *descr)
	}
}

func DisplayStartOfCaseBlank() {
	DisplayBar(80)
	return
}

func DisplayEndOfCase() {
	DisplayBar(80)
	return
}

/* ---------------------------------------------------------------- *
 * METHOD display value
 * ---------------------------------------------------------------- */

func RepresentValue(value interface{}) interface{} {
	switch reflect.TypeOf(value).Kind() {
	case reflect.Ptr:
		obj := reflect.ValueOf(value)
		if obj.IsNil() {
			return fmt.Sprintf("%v", nil)
		} else {
			return fmt.Sprintf("&%v", RepresentValue(obj.Elem().Interface()))
		}
	case reflect.Slice:
		obj := reflect.ValueOf(value)
		n := obj.Len()
		values := []interface{}{}
		for i := 0; i < n; i++ {
			x := RepresentValue(obj.Index(i).Interface())
			values = append(values, x)
		}
		if n > 10 {
			values = append(append(values[:3], "..."), values[n-2:]...)
		}
		return fmt.Sprintf("%v", values)
	case reflect.Map:
		obj := reflect.ValueOf(value)
		n := obj.Len()
		iter := obj.MapRange()
		m := map[interface{}]interface{}{}
		parts := []string{}
		for iter.Next() {
			key := iter.Key().Interface()
			x := RepresentValue(iter.Value().Interface())
			m[key] = x
			parts = append(parts, fmt.Sprintf("%[1]v: %[2]v", key, x))
		}
		if n > 4 {
			parts = append(append(parts[:3], "..."), parts[n-2:]...)
		}
		return fmt.Sprintf("{%s}", strings.Join(parts, ", "))
	}
	return value
}

/* ---------------------------------------------------------------- *
 * METHOD display algorithm start/end
 * ---------------------------------------------------------------- */

func DisplayStartOfAlgorithm(name string, inputs map[string]interface{}) {
	logging.Plain("Ausführung vom Algorithmus: \033[92;1m%[1]s\033[0m", name)
	logging.Plain("INPUTS")
	for varname, value := range inputs {
		logging.Plain("  - %[1]s = %[2]v", varname, RepresentValue(value))
	}
}

func DisplayEndOfAlgorithm(outputs map[string]interface{}) {
	logging.Plain("OUTPUTS:")
	for varname, value := range outputs {
		logging.Plain("  - %[1]s = %[2]v", varname, RepresentValue(value))
	}
}

func DisplayMetrics() {
	// logging.Plain("Dauer der Ausführung:   t  = \033[1m%[1]v\033[0m", metrics.GetTimeElapsed())
	logging.Plain("Dauer der Ausführung:   t  = \033[1m%[1]v\033[0m", metrics.GetTimeElapsedLongFormat())
	logging.Plain("Kosten (Zeit):        T(n) = \033[1m%[1]v\033[0m", displayCost(metrics.GetTimeCost()))
	logging.Plain("Kosten (#Züge):       M(n) = \033[1m%[1]v\033[0m", displayCost(metrics.GetMovesCost()))
	logging.Plain("Kosten (Platz):       S(n) = \033[1m%[1]v\033[0m", displayCost(metrics.GetSpaceCost()))
	return
}

/* ---------------------------------------------------------------- *
 * METHODS Verschiedenes
 * ---------------------------------------------------------------- */

func DisplayBar(options ...int) {
	n := 80
	if len(options) > 0 {
		n = options[0]
	}
	logging.Plain("+%[1]s+", strings.Repeat("-", n))
	return
}

func displayCost(cost int) string {
	if cost > 0 {
		return fmt.Sprintf("%v", cost)
	}
	return "-"
}
