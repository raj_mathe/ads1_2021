package menus

/* ---------------------------------------------------------------- *
 * IMPORTS
 * ---------------------------------------------------------------- */

//

/* ---------------------------------------------------------------- *
 * TYPES
 * ---------------------------------------------------------------- */

type MenuOption struct {
	Label    string
	SubLabel string
	Submenu  *Menu
	Action   func() (bool, error) // NOTE: in go, this is automatically a pointer type
}

type Menu struct {
	IsRoot         bool
	ForceReturn    bool
	Path           []string
	PromptMessages []string
	Options        [][]MenuOption
	Default        int
}

/* ---------------------------------------------------------------- *
 * TYPES - prompt
 * ---------------------------------------------------------------- */

/*
Usage

- Name: Variablenname

- Description:  Kurze Beschreibung der Variablen

- Type: Beschreiben des erwarteten Types der Variablen.

- Requirements: Liste von Anforderungen.

- Response: Ptr zur Struct, d. h. &responseType{}, wobei responseType eine struct der folgenden Form ist:

type responseType struct { Response #### `yaml:"response"` }

wobei #### = ein Typ
*/
type PromptValueQuery struct {
	Name          string
	Description   string
	Type          string
	ValidExamples *[]string
	Requirements  *[]string
	// Response muss ein Ptr auf eine Struct sein:
	Response interface{}
}
