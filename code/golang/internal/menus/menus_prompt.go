package menus

/* ---------------------------------------------------------------- *
 * IMPORTS
 * ---------------------------------------------------------------- */

import (
	"fmt"
	"strings"

	"ads/internal/core/logging"
	"ads/internal/core/utils"
)

/* ---------------------------------------------------------------- *
 * METHOD prompt options
 * ---------------------------------------------------------------- */

// Zeigt Prompt mit Liste von Optionen an
func promptListOfOptions(messages []string, options [][2]string, breaks []int, defaultChoice string) (string, bool) {
	var n = len(options)
	var (
		choice string
		cancel bool
		err    error
	)
	var lines []interface{}
	var optionsMap = map[string]string{}

	// Erzeuge Messages
	lines = []interface{}{}
	for _, message := range messages {
		lines = append(lines, message)
	}
	lines = append(lines, "")
	textbar := fmt.Sprintf(" \033[93m+%s+\033[0m", strings.Repeat("-", 40))
	lines = append(lines, textbar)
	for i, obj := range options {
		key := obj[0]
		label := obj[1]
		optionsMap[key] = label
		if key == defaultChoice {
			lines = append(lines, fmt.Sprintf("\033[91m*\033[0m\033[93;1m%v)\033[0m %v", key, label))
		} else {
			lines = append(lines, fmt.Sprintf(" \033[93;1m%v)\033[0m %v", key, label))
		}
		if i < n-1 && utils.ArrayContains(breaks, i) {
			lines = append(lines, " \033[93m+----\033[0m")
		}
		i++
	}
	lines = append(lines, textbar)
	if !(defaultChoice == "") {
		lines = append(lines, "")
		lines = append(lines, " \033[91;2m*\033[0m\033[2m = Default\033[0m")
	}

	// Wiederhole Prompt, bis gültige Eingabe erreicht
	for {
		choice, cancel, err = logging.Prompt(lines...)
		if cancel {
			return "", true
		}
		if err != nil {
			logging.ClearScreen()
			logging.Error(err)
			continue
		}
		if choice == "" {
			choice = defaultChoice
			break
		}
		if _, ok := optionsMap[choice]; !ok {
			logging.ClearScreen()
			logging.Error("Ungültige eingabe")
			continue
		}
		break
	}
	return choice, false
}
