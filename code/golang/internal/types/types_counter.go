package types

/* ---------------------------------------------------------------- *
 * IMPORTS
 * ---------------------------------------------------------------- */

import (
	"fmt"
)

/* ---------------------------------------------------------------- *
 * GLOBAL VARIABLES/CONSTANTS
 * ---------------------------------------------------------------- */

//

/* ---------------------------------------------------------------- *
 * TYPE Counter
 * ---------------------------------------------------------------- */

type Counter struct {
	nr int
}

/* ---------------------------------------------------------------- *
 * CLASS METHODS
 * ---------------------------------------------------------------- */

func NewCounter() Counter {
	c := Counter{}
	return c
}

func (self Counter) String() string {
	return fmt.Sprintf("%[1]v", self.nr)
}

func (self Counter) Size() int {
	return self.nr
}

func (self *Counter) Reset() {
	self.nr = 0
}

func (self *Counter) Set(n int) {
	self.nr = n
}

func (self *Counter) Add(options ...int) {
	n := 1
	if len(options) > 0 {
		n = options[0]
	}
	self.nr += n
}
