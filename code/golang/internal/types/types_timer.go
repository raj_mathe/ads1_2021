package types

/* ---------------------------------------------------------------- *
 * IMPORTS
 * ---------------------------------------------------------------- */

import (
	"fmt"
	"time"
)

/* ---------------------------------------------------------------- *
 * GLOBAL VARIABLES/CONSTANTS
 * ---------------------------------------------------------------- */

//

/* ---------------------------------------------------------------- *
 * TYPE Timer
 * ---------------------------------------------------------------- */

type Timer struct {
	time_elapsed time.Duration
	time_current time.Time
	running      bool
}

/* ---------------------------------------------------------------- *
 * CLASS METHODS
 * ---------------------------------------------------------------- */

func NewTimer() Timer {
	c := Timer{}
	c.Reset()
	return c
}

func (self Timer) String() string {
	return fmt.Sprintf("%[1]v", self.time_elapsed)
}

func (self Timer) CurrentTime() time.Time {
	return self.time_current
}

func (self Timer) ElapsedTime() time.Duration {
	return self.time_elapsed
}

func (self *Timer) Start() {
	self.time_current = time.Now()
	self.running = true
}

func (self *Timer) Stop() {
	if self.running {
		t0 := self.time_current
		t1 := time.Now()
		delta := t1.Sub(t0)
		self.time_current = t1
		self.time_elapsed += delta
	}
	self.running = false
}

func (self *Timer) Reset() {
	t := time.Now()
	delta := t.Sub(t) // d. h. 0
	self.time_current = t
	self.time_elapsed = delta
	self.running = false
}
