package types

/* ---------------------------------------------------------------- *
 * IMPORTS
 * ---------------------------------------------------------------- */

//

/* ---------------------------------------------------------------- *
 * YAML SCHEMES
 * ---------------------------------------------------------------- */

type AppConfig struct {
	Info    *AppConfigInfo    `yaml:"info"`
	Options *AppConfigOptions `yaml:"options"`
}

type AppConfigInfo struct {
	Title       *string `yaml:"title"`
	Description *string `yaml:"description"`
}

type AppConfigOptions struct {
	Display bool `yaml:"display"`
	Checks  bool `yaml:"checks"`
	Metrics bool `yaml:"metrics"`
}
