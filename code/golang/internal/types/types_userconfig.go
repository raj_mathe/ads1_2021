package types

/* ---------------------------------------------------------------- *
 * IMPORTS
 * ---------------------------------------------------------------- */

//

/* ---------------------------------------------------------------- *
 * YAML SCHEMES
 * ---------------------------------------------------------------- */

type UserConfig struct {
	Info  *UserConfigInfo  `yaml:"info"`
	Parts *UserConfigParts `yaml:"parts"`
}

type UserConfigInfo struct {
	Title       *string `yaml:"title"`
	Description *string `yaml:"description"`
}

type UserConfigParts struct {
	Cases *([]UserConfigCase) `yaml:"cases"`
}

type UserConfigCase struct {
	Command     *string           `yaml:"command"`
	Description *string           `yaml:"description"`
	Inputs      *UserConfigInputs `yaml:"inputs"`
}

type UserConfigInputs struct {
	List        *[]int `yaml:"L"`
	SearchRank  *int   `yaml:"i"`
	SearchValue *int   `yaml:"x"`
	JumpSize    *int   `yaml:"m"`
}
