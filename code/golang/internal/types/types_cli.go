package types

/* ---------------------------------------------------------------- *
 * IMPORTS
 * ---------------------------------------------------------------- */

import (
	"github.com/akamensky/argparse"

	"ads/internal/core/utils"
)

/* ---------------------------------------------------------------- *
 * TYPES
 * ---------------------------------------------------------------- */

type CliArguments struct {
	ModeHelp    *argparse.Command
	ModeVersion *argparse.Command
	ModeRun     *argparse.Command
	Quiet       *bool
	Debug       *bool
	Interactive *bool
	Checks      *string
	Colour      *string
	ConfigFile  *string
}

/* ---------------------------------------------------------------- *
 * METHODS convert string option to boolean
 * ---------------------------------------------------------------- */

func (arguments CliArguments) QuietModeOn() bool {
	return *arguments.Quiet
}

func (arguments CliArguments) DebugModeOn() bool {
	return *arguments.Debug
}

func (arguments CliArguments) InteractiveMode() bool {
	return *arguments.Interactive
}

func (arguments CliArguments) ShowChecks() bool {
	return !utils.IsFalse(*arguments.Checks)
}

func (arguments CliArguments) ShowColour() bool {
	return !utils.IsFalse(*arguments.Colour)
}

func (arguments CliArguments) GetConfigFile() string {
	return *arguments.ConfigFile
}
