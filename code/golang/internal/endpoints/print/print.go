package print

/* ---------------------------------------------------------------- *
 * IMPORTS
 * ---------------------------------------------------------------- */

import (
	"ads/internal/core/logging"
	"ads/internal/setup"
	"ads/internal/setup/cli"
)

/* ---------------------------------------------------------------- *
 * ENDPOINT version
 * ---------------------------------------------------------------- */

func Version() {
	logging.SetForce(true)
	logging.Plain(setup.Version())
}

/* ---------------------------------------------------------------- *
 * ENDPOINT help
 * ---------------------------------------------------------------- */

func Help() {
	logging.SetForce(true)
	cli.ParseCli([]string{}) // <- generiere leere Instanz für Parser, um voll Hilfsanleitug zu zeigen
	logging.Plain("")
	logging.Plain(setup.Logo())
	// logging.Plain(setup.Help())
	logging.Plain(cli.Parser.Usage(nil))
	logging.Plain("")
}
