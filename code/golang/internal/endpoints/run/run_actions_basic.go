package run

/* ---------------------------------------------------------------- *
 * IMPORTS
 * ---------------------------------------------------------------- */

import (
	"ads/internal/core/logging"
	"ads/internal/setup"
)

/* ---------------------------------------------------------------- *
 * ACTIONS - basic
 * ---------------------------------------------------------------- */

func actionShowVersion() (bool, error) {
	logging.Plain("")
	logging.Plain(setup.Logo())
	logging.Plain("Version: \033[1m%s\033[0m", setup.Version())
	logging.Plain("")
	return false, nil
}

func actionRunOnConfig() (bool, error) {
	path, cancel, err := logging.Prompt("Pfad zur Configdatei bitte eingeben:")
	if cancel {
		return true, nil
	}
	if err != nil {
		return false, err
	}
	err = RunNonInteractive(path)
	return false, err
}
