package run

/* ---------------------------------------------------------------- *
 * IMPORTS
 * ---------------------------------------------------------------- */

import (
	"ads/internal/core/logging"
	"ads/internal/setup"
)

/* ---------------------------------------------------------------- *
 * ACTIONS - settting
 * ---------------------------------------------------------------- */

func actionShowSettings() (bool, error) {
	var state string
	state = "aus"
	if logging.GetAnsiMode() {
		state = "ein"
	}
	logging.Info("Farbenmodus: %s", state)
	state = "aus"
	if logging.GetDebugMode() {
		state = "ein"
	}
	logging.Info("Debugmodus: %s", state)
	state = "aus"
	if setup.AppConfigPerformChecks() {
		state = "ein"
	}
	logging.Info("Pre/Postchecking: %s", state)
	return false, nil
}

func actionColourModeOn() (bool, error) {
	logging.SetAnsiMode(true)
	return false, nil
}

func actionColourModeOff() (bool, error) {
	logging.SetAnsiMode(false)
	return false, nil
}

func actionDebugModeOn() (bool, error) {
	logging.SetDebugMode(true)
	return false, nil
}

func actionDebugModeOff() (bool, error) {
	logging.SetDebugMode(false)
	return false, nil
}

func actionPrePostCheckingOn() (bool, error) {
	setup.SetAppConfigPerformChecks(true)
	return false, nil
}

func actionPrePostCheckingOff() (bool, error) {
	setup.SetAppConfigPerformChecks(false)
	return false, nil
}
