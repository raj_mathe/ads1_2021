package run

/* ---------------------------------------------------------------- *
 * IMPORTS
 * ---------------------------------------------------------------- */

import (
	"ads/internal/menus"
)

/* ---------------------------------------------------------------- *
 * CUSTOM PROMPTS für besondere Inputs
 * ---------------------------------------------------------------- */

func promptInputInteger(name string, descr string, requirements []string) (int, bool, error) {
	type responseType struct {
		Response int `yaml:"response"`
	}
	var response = responseType{}
	var query = menus.PromptValueQuery{
		Description:  descr,
		Name:         name,
		Type:         "Integer",
		Requirements: &requirements,
		Response:     &response,
	}
	cancel, err := query.Prompt()
	return response.Response, cancel, err
}

func promptInputListOfInt(name string, descr string, requirements []string) ([]int, bool, error) {
	type responseType struct {
		Response []int `yaml:"response"`
	}
	var response = responseType{}
	var query = menus.PromptValueQuery{
		Description: descr,
		Name:        name,
		Type:        "Integerliste",
		ValidExamples: &[]string{
			"[1, 2, 7, 8, 5]",
			"[1000, 0, 4]",
		},
		Requirements: &requirements,
		Response:     &response,
	}
	cancel, err := query.Prompt()
	return response.Response, cancel, err
}

func promptInputListOfZeroOnes(name string, descr string, requirements []string) ([]int, bool, error) {
	var values = []int{}
	type responseType struct {
		Response []uint8 `yaml:"response"`
	}
	var response = responseType{}
	var query = menus.PromptValueQuery{
		Description: descr,
		Name:        name,
		Type:        "Liste von 0er und 1er",
		ValidExamples: &[]string{
			"[0, 0, 0, 1, 0]",
			"[1, 0, 1, 1]",
		},
		Requirements: &requirements,
		Response:     &response,
	}
	cancel, err := query.Prompt()
	// uint8 -> int
	if response.Response != nil {
		for _, x := range response.Response {
			values = append(values, int(x))
		}
	}
	return values, cancel, err
}
