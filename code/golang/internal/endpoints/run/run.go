package run

/* ---------------------------------------------------------------- *
 * IMPORTS
 * ---------------------------------------------------------------- */

import (
	"fmt"

	"ads/internal/core/logging"
	"ads/internal/setup"
	"ads/internal/types"

	algorithm_search_binary "ads/pkg/algorithms/search/binary"
	algorithm_search_interpol "ads/pkg/algorithms/search/interpol"
	algorithm_search_ith_element "ads/pkg/algorithms/search/ith_element"
	algorithm_search_jump "ads/pkg/algorithms/search/jump"
	algorithm_search_poison "ads/pkg/algorithms/search/poison"
	algorithm_search_sequential "ads/pkg/algorithms/search/sequential"
	algorithm_stacks_next_greater_element "ads/pkg/algorithms/stacks/next_greater_element"
	algorithm_sum_maxsubsum "ads/pkg/algorithms/sum/maxsubsum"
)

/* ---------------------------------------------------------------- *
 * ENDPOINT run interactive modus
 * ---------------------------------------------------------------- */

// Startet App im interaktiven Modus
func RunInteractive() error {
	logging.Plain(setup.Logo())
	_, err := menuMain.ShowMenu()
	logging.Plain("\033[2;3m...Programm terminiert.\033[0m")
	return err
}

/* ---------------------------------------------------------------- *
 * ENDPOINT run non-interactive modus
 * ---------------------------------------------------------------- */

// Liest Config Datei ein und führt Algorithmen auf Fälle durch
func RunNonInteractive(path string) error {
	var err error
	var err_case error

	// extrahiere user config
	config := setup.NewUserConfig()
	err = setup.GetUserConfig(path, &config)
	if err != nil {
		return err
	}

	logging.Plain(setup.Logo())

	// Fälle extrahieren
	cases := []types.UserConfigCase{}
	if config.Parts != nil && config.Parts.Cases != nil {
		cases = *config.Parts.Cases
	}
	for i := 0; i < len(cases); i++ {
		err_case = nil
		problem := cases[i]
		setup.DisplayStartOfCase(i, problem.Description)
		inputs := types.UserConfigInputs{}
		if problem.Inputs != nil {
			inputs = *problem.Inputs
		}
		if problem.Command == nil {
			err_case = fmt.Errorf("Attribute 'command:' fehlt!")
		} else {
			switch *problem.Command {
			case "algorithm-search-binary":
				L := inputs.List
				x := inputs.SearchValue
				if L != nil && x != nil {
					_, err_case = algorithm_search_binary.FancyBinarySearch(*L, *x)
				} else {
					err_case = fmt.Errorf("Fehlende Inputs für Befehl '%[1]s'.", *problem.Command)
				}
			case "algorithm-search-interpolation":
				L := inputs.List
				x := inputs.SearchValue
				if L != nil && x != nil {
					_, err_case = algorithm_search_interpol.FancyInterpolationSearch(*L, *x, 0, len(*L)-1)
				} else {
					err_case = fmt.Errorf("Fehlende Inputs für Befehl '%[1]s'.", *problem.Command)
				}
			case "algorithm-search-ith-element":
				L := inputs.List
				i := inputs.SearchRank
				if L != nil && i != nil {
					_, err_case = algorithm_search_ith_element.FancyFindIthSmallest(*L, *i)
				} else {
					err_case = fmt.Errorf("Fehlende Inputs für Befehl '%[1]s'.", *problem.Command)
				}
			case "algorithm-search-ith-element-dc":
				L := inputs.List
				i := inputs.SearchRank
				if L != nil && i != nil {
					_, err_case = algorithm_search_ith_element.FancyFindIthSmallestDC(*L, *i)
				} else {
					err_case = fmt.Errorf("Fehlende Inputs für Befehl '%[1]s'.", *problem.Command)
				}
			case "algorithm-search-jump":
				L := inputs.List
				x := inputs.SearchValue
				m := inputs.JumpSize
				if L != nil && x != nil {
					_, err_case = algorithm_search_jump.FancyJumpSearchLinear(*L, *x, *m)
				} else {
					err_case = fmt.Errorf("Fehlende Inputs für Befehl '%[1]s'.", *problem.Command)
				}
			case "algorithm-search-jump-exp":
				L := inputs.List
				x := inputs.SearchValue
				if L != nil && x != nil {
					_, err_case = algorithm_search_jump.FancyJumpSearchExponentiell(*L, *x)
				} else {
					err_case = fmt.Errorf("Fehlende Inputs für Befehl '%[1]s'.", *problem.Command)
				}
			case "algorithm-search-poison":
				L := inputs.List
				if L != nil {
					_, err_case = algorithm_search_poison.FancyFindPoison(*L)
				} else {
					err_case = fmt.Errorf("Fehlende Inputs für Befehl '%[1]s'.", *problem.Command)
				}
			case "algorithm-search-poison-fast":
				L := inputs.List
				if L != nil {
					_, err_case = algorithm_search_poison.FancyFindPoisonFast(*L)
				} else {
					err_case = fmt.Errorf("Fehlende Inputs für Befehl '%[1]s'.", *problem.Command)
				}
			case "algorithm-search-sequential":
				L := inputs.List
				x := inputs.SearchValue
				if L != nil && x != nil {
					_, err_case = algorithm_search_sequential.FancySequentialSearch(*L, *x)
				} else {
					err_case = fmt.Errorf("Fehlende Inputs für Befehl '%[1]s'.", *problem.Command)
				}
			case "algorithm-stacks-next-greater-element":
				L := inputs.List
				if L != nil {
					_, err_case = algorithm_stacks_next_greater_element.FancyNextGreaterElement(*L)
				} else {
					err_case = fmt.Errorf("Fehlende Inputs für Befehl '%[1]s'.", *problem.Command)
				}
			case "algorithm-sum-maxsub":
				L := inputs.List
				if L != nil {
					_, _, _, err_case = algorithm_sum_maxsubsum.FancyMaxSubSum(*L)
				} else {
					err_case = fmt.Errorf("Fehlende Inputs für Befehl '%[1]s'.", *problem.Command)
				}
			case "algorithm-sum-maxsub-dc":
				L := inputs.List
				if L != nil {
					_, _, _, err_case = algorithm_sum_maxsubsum.FancyMaxSubSumDC(*L)
				} else {
					err_case = fmt.Errorf("Fehlende Inputs für Befehl '%[1]s'.", *problem.Command)
				}
			default:
				err_case = fmt.Errorf("Unbekannter Befehl '%[1]s'.", *problem.Command)
			}
		}
		if err_case != nil {
			logging.Error(err_case)
		}
	}
	setup.DisplayEndOfCase()
	return nil
}
