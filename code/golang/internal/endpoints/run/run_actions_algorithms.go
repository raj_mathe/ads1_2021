package run

/* ---------------------------------------------------------------- *
 * IMPORTS
 * ---------------------------------------------------------------- */

import (
	"ads/internal/core/logging"
	"ads/internal/setup"

	algorithm_search_binary "ads/pkg/algorithms/search/binary"
	algorithm_search_interpol "ads/pkg/algorithms/search/interpol"
	algorithm_search_ith_element "ads/pkg/algorithms/search/ith_element"
	algorithm_search_jump "ads/pkg/algorithms/search/jump"
	algorithm_search_poison "ads/pkg/algorithms/search/poison"
	algorithm_search_sequential "ads/pkg/algorithms/search/sequential"
	algorithm_stacks_next_greater_element "ads/pkg/algorithms/stacks/next_greater_element"
	algorithm_sum_maxsubsum "ads/pkg/algorithms/sum/maxsubsum"
)

/* ---------------------------------------------------------------- *
 * ACTIONS algorithmen - search
 * ---------------------------------------------------------------- */

func actionAlgorithmSearchBinary() (bool, error) {
	input_L, cancel, err := promptInputListOfInt("L", "Liste von Werten", []string{
		"muss aufsteigend sortiert sein",
	})
	if cancel || err != nil {
		return cancel, err
	}
	input_x, cancel, err := promptInputInteger("x", "Suchwert", []string{})
	if cancel || err != nil {
		return cancel, err
	}
	setup.DisplayStartOfCaseBlank()
	_, err = algorithm_search_binary.FancyBinarySearch(input_L, input_x)
	if err != nil {
		logging.Error(err)
	}
	setup.DisplayEndOfCase()
	return cancel, nil
}

func actionAlgorithmSearchInterpolation() (bool, error) {
	input_L, cancel, err := promptInputListOfInt("L", "Liste von Werten", []string{
		"muss aufsteigend sortiert sein",
		"sollte (idealerweise) nicht uniform verteilt sein",
	})
	if cancel || err != nil {
		return cancel, err
	}
	input_x, cancel, err := promptInputInteger("x", "Suchwert", []string{})
	if cancel || err != nil {
		return cancel, err
	}
	setup.DisplayStartOfCaseBlank()
	_, err = algorithm_search_interpol.FancyInterpolationSearch(input_L, input_x, 0, len(input_L)-1)
	if err != nil {
		logging.Error(err)
	}
	setup.DisplayEndOfCase()
	return cancel, nil
}

func actionAlgorithmSearchIthElement() (bool, error) {
	input_L, cancel, err := promptInputListOfInt("L", "Liste von Werten", []string{})
	if cancel || err != nil {
		return cancel, err
	}
	input_i, cancel, err := promptInputInteger("i", "Suchindex in sortierter Liste", []string{
		"sollte zw. 1 und len(L) liegen",
	})
	if cancel || err != nil {
		return cancel, err
	}
	setup.DisplayStartOfCaseBlank()
	_, err = algorithm_search_ith_element.FancyFindIthSmallest(input_L, input_i)
	if err != nil {
		logging.Error(err)
	}
	setup.DisplayEndOfCase()
	return cancel, nil
}

func actionAlgorithmSearchIthElementDc() (bool, error) {
	input_L, cancel, err := promptInputListOfInt("L", "Liste von Werten", []string{})
	if cancel || err != nil {
		return cancel, err
	}
	input_i, cancel, err := promptInputInteger("i", "Suchindex in sortierter Liste", []string{
		"sollte zw. 1 und len(L) liegen",
	})
	if cancel || err != nil {
		return cancel, err
	}
	setup.DisplayStartOfCaseBlank()
	_, err = algorithm_search_ith_element.FancyFindIthSmallestDC(input_L, input_i)
	if err != nil {
		logging.Error(err)
	}
	setup.DisplayEndOfCase()
	return cancel, nil
}

func actionAlgorithmSearchJump() (bool, error) {
	input_L, cancel, err := promptInputListOfInt("L", "Liste von Werten", []string{
		"muss aufsteigend sortiert sein",
		"sollte (idealerweise) nicht uniform verteilt sein",
		"sollte (idealerweise) keine Duplikate enthalten",
	})
	if cancel || err != nil {
		return cancel, err
	}
	input_x, cancel, err := promptInputInteger("x", "Suchwert", []string{})
	if cancel || err != nil {
		return cancel, err
	}
	input_m, cancel, err := promptInputInteger("m", "Sprunggröße", []string{
		"sollte >= 2 sein",
	})
	if cancel || err != nil {
		return cancel, err
	}
	setup.DisplayStartOfCaseBlank()
	_, err = algorithm_search_jump.FancyJumpSearchLinear(input_L, input_x, input_m)
	if err != nil {
		logging.Error(err)
	}
	setup.DisplayEndOfCase()
	return cancel, nil
}

func actionAlgorithmSearchJumpExp() (bool, error) {
	input_L, cancel, err := promptInputListOfInt("L", "Liste von Werten", []string{
		"muss aufsteigend sortiert sein",
		"sollte (idealerweise) nicht uniform verteilt sein",
		"sollte (idealerweise) keine Duplikate enthalten",
	})
	if cancel || err != nil {
		return cancel, err
	}
	input_x, cancel, err := promptInputInteger("x", "Suchwert", []string{})
	if cancel || err != nil {
		return cancel, err
	}
	setup.DisplayStartOfCaseBlank()
	_, err = algorithm_search_jump.FancyJumpSearchExponentiell(input_L, input_x)
	if err != nil {
		logging.Error(err)
	}
	setup.DisplayEndOfCase()
	return cancel, nil
}

func actionAlgorithmSearchPoison() (bool, error) {
	input_L, cancel, err := promptInputListOfZeroOnes("L", "Liste von Werten", []string{
		"muss genau eine 1 enthalten",
	})
	if cancel || err != nil {
		return cancel, err
	}
	setup.DisplayStartOfCaseBlank()
	_, err = algorithm_search_poison.FancyFindPoison(input_L)
	if err != nil {
		logging.Error(err)
	}
	setup.DisplayEndOfCase()
	return cancel, nil
}

func actionAlgorithmSearchPoisonFast() (bool, error) {
	input_L, cancel, err := promptInputListOfZeroOnes("L", "Liste von Getränken", []string{
		"muss genau eine 1 enthalten",
	})
	if cancel || err != nil {
		return cancel, err
	}
	setup.DisplayStartOfCaseBlank()
	_, err = algorithm_search_poison.FancyFindPoisonFast(input_L)
	if err != nil {
		logging.Error(err)
	}
	setup.DisplayEndOfCase()
	return cancel, nil
}

func actionAlgorithmSearchSequential() (bool, error) {
	input_L, cancel, err := promptInputListOfInt("L", "Liste von Werten", []string{})
	if cancel || err != nil {
		return cancel, err
	}
	input_x, cancel, err := promptInputInteger("x", "Suchwert", []string{})
	if cancel || err != nil {
		return cancel, err
	}
	setup.DisplayStartOfCaseBlank()
	_, err = algorithm_search_sequential.FancySequentialSearch(input_L, input_x)
	if err != nil {
		logging.Error(err)
	}
	setup.DisplayEndOfCase()
	return cancel, nil
}

/* ---------------------------------------------------------------- *
 * ACTIONS algorithmen - stacks
 * ---------------------------------------------------------------- */

func actionAlgorithmStacksNextGreaterElement() (bool, error) {
	input_L, cancel, err := promptInputListOfInt("L", "Liste von Werten", []string{})
	if cancel || err != nil {
		return cancel, err
	}
	setup.DisplayStartOfCaseBlank()
	_, err = algorithm_stacks_next_greater_element.FancyNextGreaterElement(input_L)
	if err != nil {
		logging.Error(err)
	}
	setup.DisplayEndOfCase()
	return cancel, nil
}

/* ---------------------------------------------------------------- *
 * ACTIONS algorithmen - sum
 * ---------------------------------------------------------------- */

func actionAlgorithmSumMaxsub() (bool, error) {
	input_L, cancel, err := promptInputListOfInt("L", "Liste von Werten", []string{})
	if cancel || err != nil {
		return cancel, err
	}
	setup.DisplayStartOfCaseBlank()
	_, _, _, err = algorithm_sum_maxsubsum.FancyMaxSubSum(input_L)
	if err != nil {
		logging.Error(err)
	}
	setup.DisplayEndOfCase()
	return cancel, nil
}

func actionAlgorithmSumMaxsubDc() (bool, error) {
	input_L, cancel, err := promptInputListOfInt("L", "Liste von Werten", []string{})
	if cancel || err != nil {
		return cancel, err
	}
	setup.DisplayStartOfCaseBlank()
	_, _, _, err = algorithm_sum_maxsubsum.FancyMaxSubSumDC(input_L)
	if err != nil {
		logging.Error(err)
	}
	setup.DisplayEndOfCase()
	return cancel, nil
}
