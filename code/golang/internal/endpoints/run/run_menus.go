package run

/* ---------------------------------------------------------------- *
 * IMPORTS
 * ---------------------------------------------------------------- */

import (
	"ads/internal/menus"
)

/* ---------------------------------------------------------------- *
 * MENUS
 * ---------------------------------------------------------------- */

var menuMain = menus.Menu{
	IsRoot: true,
	Path:   []string{"Hauptmenü"},
	PromptMessages: []string{
		"Option wählen",
	},
	Options: [][]menus.MenuOption{
		{
			{Label: "Einstellungen", Submenu: &menuSettings},
			{Label: "Version des Programms anzeigen", Action: actionShowVersion},
		}, {
			{Label: "Programm auf config Datei ausführen.", Action: actionRunOnConfig},
		}, {
			{Label: "Suchalgorithmen", Submenu: &menuSearchAlgorithms},
			{Label: "Summenalgorithmen", Submenu: &menuSumAlgorithms},
			{Label: "Algorithmen mit Stacks und Queues", Submenu: &menuStacksQueuesAlgorithms},
		},
	},
	Default: -1,
}

var menuSettings = menus.Menu{
	Path: []string{"Hauptmenü", "Einstellungen"},
	PromptMessages: []string{
		"Option wählen",
	},
	Options: [][]menus.MenuOption{
		{
			{Label: "Aktueller Zustand", Action: actionShowSettings},
			{Label: "Farbenmodus", Submenu: &menuColourMode},
			{Label: "Debugmodus", Submenu: &menuDebugMode},
			{Label: "Pre / Postchecking", Submenu: &menuPrePostChecking},
		},
	},
	Default: -1,
}

var menuColourMode = menus.Menu{
	ForceReturn: true,
	Path:        []string{"Hauptmenü", "Einstellungen", "Farbenmodus"},
	PromptMessages: []string{
		"Soll Console-Output mit Formattierung dargestellt werden?",
		"Option für den Farbenmodus wählen:",
	},
	Options: [][]menus.MenuOption{
		{
			{Label: "ein", Action: actionColourModeOn},
			{Label: "aus", Action: actionColourModeOff},
		},
	},
	Default: 1,
}

var menuDebugMode = menus.Menu{
	ForceReturn: true,
	Path:        []string{"Hauptmenü", "Einstellungen", "Debugmodus"},
	PromptMessages: []string{
		"Sollen Infos zu jedem Schritt der Algorithmen angezeigt werden?",
		"Option für den Debugmodus wählen:",
	},
	Options: [][]menus.MenuOption{
		{
			{Label: "ein", Action: actionDebugModeOn},
			{Label: "aus", Action: actionDebugModeOff},
		},
	},
	Default: 0,
}

var menuPrePostChecking = menus.Menu{
	ForceReturn: true,
	Path:        []string{"Hauptmenü", "Einstellungen", "Pre/Postchecking"},
	PromptMessages: []string{
		"Sollen Inputs+Outputs bei Algorithmenausführungen geprüft werden?",
		"Option für den Pre/Postchecking wählen:",
	},
	Options: [][]menus.MenuOption{
		{
			{Label: "ein", Action: actionPrePostCheckingOn},
			{Label: "aus", Action: actionPrePostCheckingOff},
		},
	},
	Default: 1,
}

var menuSearchAlgorithms = menus.Menu{
	Path: []string{"Hauptmenü", "Suchalgorithmen"},
	PromptMessages: []string{
		"Algorithmus wählen",
	},
	Options: [][]menus.MenuOption{
		{
			{Label: "Binärsuchalgorithmus", Action: actionAlgorithmSearchBinary},
			{Label: "Interpolationsalgorithmus", Action: actionAlgorithmSearchInterpolation},
			{Label: "Suche i. kleinstes Element", Action: actionAlgorithmSearchIthElement},
			{Label: "Suche i. kleinstes Element", SubLabel: "D & C", Action: actionAlgorithmSearchIthElementDc},
			{Label: "Sprungsuche", SubLabel: "linear", Action: actionAlgorithmSearchJump},
			{Label: "Sprungsuche", SubLabel: "exponentiell", Action: actionAlgorithmSearchJumpExp},
			{Label: "Giftsuche", Action: actionAlgorithmSearchPoison},
			{Label: "Giftsuche", SubLabel: "optimiert", Action: actionAlgorithmSearchPoisonFast},
			{Label: "Sequentiellsuchalgorithmus", Action: actionAlgorithmSearchSequential},
		},
	},
	Default: -1,
}

var menuStacksQueuesAlgorithms = menus.Menu{
	Path: []string{"Hauptmenü", "Algorithmen mit Stacks und Queues"},
	PromptMessages: []string{
		"Algorithmus wählen",
	},
	Options: [][]menus.MenuOption{
		{
			{Label: "'Next Greater Element' mit Stacks", Action: actionAlgorithmStacksNextGreaterElement},
		},
	},
	Default: -1,
}

var menuSumAlgorithms = menus.Menu{
	Path: []string{"Hauptmenü", "Summenalgorithmen"},
	PromptMessages: []string{
		"Algorithmus wählen",
	},
	Options: [][]menus.MenuOption{
		{
			{Label: "Maximale Teilsumme", SubLabel: "brute force", Action: actionAlgorithmSumMaxsub},
			{Label: "Maximale Teilsumme", SubLabel: "D & C", Action: actionAlgorithmSumMaxsubDc},
		},
	},
	Default: -1,
}
