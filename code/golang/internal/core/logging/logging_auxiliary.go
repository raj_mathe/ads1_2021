package logging

/* ---------------------------------------------------------------- *
 * IMPORTS
 * ---------------------------------------------------------------- */

import (
	"fmt"
	"os"

	"ads/internal/core/utils"
)

/* ---------------------------------------------------------------- *
 * AUXILIARY METHODS
 * ---------------------------------------------------------------- */

func logGeneric(pipe *os.File, tag string, line interface{}, args ...interface{}) {
	if !force && quietmode {
		return
	}
	if !(tag == "") {
		tag = tag + " "
	}
	fmt.Fprintln(pipe, decorateLine(loggingPrefix, tag, expandLine(line, args)))
}

func expandLine(line interface{}, args []interface{}) string {
	return fmt.Sprintf(fmt.Sprintf("%s", line), args...)
}

func decorateLine(loggingPrefix string, tag string, line string) string {
	return stripAnsiIfNecessary(fmt.Sprintf("%[1]s%[2]s%[3]v", loggingPrefix, tag, line))
}

func stripAnsiIfNecessary(line string) string {
	if !ansimode {
		line = utils.StripAnsi(line)
	}
	return line
}

func ClearScreen() {
	fmt.Print("\033[2J")
}
