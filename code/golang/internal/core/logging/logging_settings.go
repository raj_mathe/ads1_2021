package logging

/* ---------------------------------------------------------------- *
 * IMPORTS
 * ---------------------------------------------------------------- */

/* ---------------------------------------------------------------- *
 * GLOBAL VARIABLES
 * ---------------------------------------------------------------- */

var quietmode bool = false
var debugmode bool = true
var ansimode bool = false
var loggingPrefix string = ""
var force bool = false
var promptSymb string = ">"

func GetQuietMode() bool {
	return quietmode
}

func SetQuietMode(mode bool) {
	quietmode = mode
}

func GetDebugMode() bool {
	return debugmode
}

func SetDebugMode(mode bool) {
	debugmode = mode
}

func GetAnsiMode() bool {
	return ansimode
}

func SetAnsiMode(mode bool) {
	ansimode = mode
}

func GetForce() bool {
	return force
}

func SetForce(mode bool) {
	force = mode
}
