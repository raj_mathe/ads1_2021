package logging

/* ---------------------------------------------------------------- *
 * IMPORTS
 * ---------------------------------------------------------------- */

import (
	"fmt"
	"os"
)

/* ---------------------------------------------------------------- *
 * MAIN METHODS logging
 * ---------------------------------------------------------------- */

func Plain(line interface{}, args ...interface{}) {
	logGeneric(os.Stdout, "", line, args...)
}

func Info(line interface{}, args ...interface{}) {
	logGeneric(os.Stdout, "[\033[94;1mINFO\033[0m]", line, args...)
}

func Debug(line interface{}, args ...interface{}) {
	if !debugmode {
		return
	}
	logGeneric(os.Stdout, "\033[2m[\033[96;1mDEBUG\033[0m\033[2m]\033[0m", fmt.Sprintf("\033[2m%v\033[0m", line), args...)
}

func Warn(line interface{}, args ...interface{}) {
	logGeneric(os.Stdout, "[\033[93;1mWARNING\033[0m]", line, args...)
}

func Error(line interface{}, args ...interface{}) {
	logGeneric(os.Stderr, "[\033[91;1mERROR\033[0m]", line, args...)
}

func Fatal(line interface{}, args ...interface{}) {
	logGeneric(os.Stderr, "[\033[91;1mFATAL\033[0m]", line, args...)
	os.Exit(1)
}
