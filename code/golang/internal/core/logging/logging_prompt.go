package logging

/* ---------------------------------------------------------------- *
 * IMPORTS
 * ---------------------------------------------------------------- */

import (
	"bufio"
	"fmt"
	"io"
	"os"

	"ads/pkg/re"
)

/* ---------------------------------------------------------------- *
 * METHOD prompts
 * ---------------------------------------------------------------- */

// Zeigt Prompt an und liest Usereingabe aus, erkennt auch ob Meta+D geklickt wurde
func Prompt(lines ...interface{}) (string, bool, error) {
	pipe := os.Stdout
	if len(lines) > 0 {
		tag := ""
		for _, line := range lines {
			logGeneric(pipe, tag, line)
		}
		logGeneric(pipe, tag, "")
	}
	fmt.Fprintf(pipe, "%s ", promptSymb)
	reader := bufio.NewReader(os.Stdin)
	line, err := reader.ReadString('\n')
	line = re.Sub(`\s+$`, "", line)
	if err != nil && err == io.EOF {
		fmt.Fprintln(pipe, "")
		return line, true, err
	}
	return line, false, err
}

func PromptAnyKeyToContinue() bool {
	pipe := os.Stdout
	fmt.Fprint(pipe, stripAnsiIfNecessary("\033[2;3mEingabetaste (Enter) zum Fortsetzen drücken:\033[0m "))
	_, exit, _ := Prompt()
	return exit
}
