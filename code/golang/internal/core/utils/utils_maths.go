package utils

/*
In Golang sind manche (basic!) mathematischen Vorgänge etwas umständlich zu implementieren.
Wir wollen diese Methoden komplett einhüllen, damit wir von ihrer Komplexität
in den Algorithmen nicht abgelenkt werden.
*/

/* ---------------------------------------------------------------- *
 * IMPORTS
 * ---------------------------------------------------------------- */

import (
	"fmt"
	"math"
	"strconv"
)

/* ---------------------------------------------------------------- *
 * METHOD floor, ceil
 * ---------------------------------------------------------------- */

func Floor(x float64) int {
	return int(math.Floor(x))
}

func Ceil(x float64) int {
	return int(math.Ceil(x))
}

/* ---------------------------------------------------------------- *
 * METHOD max, min
 * ---------------------------------------------------------------- */

func MinInt(x int, y ...int) int {
	if len(y) == 0 {
		return x
	} else if x < y[0] {
		return MinInt(x, y[1:]...)
	} else {
		return MinInt(y[0], y[1:]...)
	}
}

func MaxInt(x int, y ...int) int {
	if len(y) == 0 {
		return x
	} else if x > y[0] {
		return MaxInt(x, y[1:]...)
	} else {
		return MaxInt(y[0], y[1:]...)
	}
}

func ArgMinInt(X []int) int {
	if len(X) == 0 {
		return -1
	}
	minValue := X[0]
	index := 0
	for i, x := range X {
		if x < minValue {
			minValue = x
			index = i
		}
	}
	return index
}

func ArgMaxInt(X []int) int {
	if len(X) == 0 {
		return -1
	}
	maxValue := X[0]
	index := 0
	for i, x := range X {
		if x > maxValue {
			maxValue = x
			index = i
		}
	}
	return index
}

/* ---------------------------------------------------------------- *
 * METHOD sum
 * ---------------------------------------------------------------- */

func SumListInt(X []int) int {
	result := 0
	for _, x := range X {
		result += x
	}
	return result
}

func SumListFloat(X []float64) float64 {
	result := 0.
	for _, x := range X {
		result += x
	}
	return result
}

/* ---------------------------------------------------------------- *
 * METHODS binary
 * ---------------------------------------------------------------- */

/*
Für eine Zahl n != 0 liefert die Länge der binären Darstellung der Zahl,
und für n == 0 liefert 0
*/
func LengthOfBinary(number int) int {
	if number == 0 {
		return 0
	}
	if number < 0 {
		number = -number
	}
	number_binary := fmt.Sprintf("%b", int64(number))
	return len([]rune(number_binary))
}

/*
Für eine Zahl n != 0 liefert die Bits der binären Darstellung
*/
func IntToBinary(number int) []int {
	if number == 0 || number == 1 {
		return []int{number}
	}
	number_binary := fmt.Sprintf("%b", int64(number))
	runes := []rune(number_binary)
	bits := make([]int, len(runes))
	for i, rune := range runes {
		bit, _ := strconv.ParseInt(fmt.Sprintf("%c", rune), 2, 64)
		bits[len(runes)-1-i] = int(bit)
	}
	return bits
}

func NthBit(number int, digit int) int {
	bits := IntToBinary(number)
	if digit < len(bits) {
		return bits[digit]
	}
	return 0
}
