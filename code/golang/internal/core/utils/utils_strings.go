package utils

/* ---------------------------------------------------------------- *
 * IMPORTS
 * ---------------------------------------------------------------- */

import (
	"fmt"
	"log"
	"strconv"
	"strings"

	"github.com/lithammer/dedent"

	"ads/pkg/re"
)

/* ---------------------------------------------------------------- *
 * METHOD dedent textblock and expand escaped symbols
 * ---------------------------------------------------------------- */

func DedentIgnoreEmptyLines(text string) string {
	return dedent.Dedent(text)
}

func DedentIgnoreFirstAndLast(text string) string {
	text = re.Sub(`^\s*[\n\r]|[\n\r]\s*$`, ``, text)
	return DedentIgnoreEmptyLines(text)
}

func DedentAndExpand(text string) string {
	var err error
	var result []string
	result = []string{}
	text = dedent.Dedent(text)
	lines := strings.Split(text, "\n")
	for _, line := range lines {
		line = fmt.Sprintf(`"%s"`, line)
		line, err = strconv.Unquote(line)
		if err != nil {
			log.Fatal(err)
		}
		result = append(result, line)
	}
	return strings.Join(result, "\n")
}

func FormatTextBlockAsList(text string, options ...bool) []string {
	var unindent bool = GetArrayBoolValue(&options, 0, true)
	if unindent {
		text = DedentIgnoreFirstAndLast(text)
	}
	return re.Split(`\n`, text)
}

/* ---------------------------------------------------------------- *
 * METHODS ansi
 * ---------------------------------------------------------------- */

func StripAnsi(text string) string {
	return re.Sub(`\x1b[^m]*m`, ``, text)
}

/* ---------------------------------------------------------------- *
 * METHODS string -> bool
 * ---------------------------------------------------------------- */

func IsTrue(text string) bool {
	text = strings.TrimSpace(text)
	return re.Matches(`(?i)(^(true|t|yes|y|1|\+|\+1)$)`, text)
}

func IsFalse(text string) bool {
	text = strings.TrimSpace(text)
	return re.Matches(`(?i)(^(false|f|no|n|0|-|-1)$)`, text)
}
