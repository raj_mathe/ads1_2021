package utils

/* ---------------------------------------------------------------- *
 * IMPORTS
 * ---------------------------------------------------------------- */

import (
	"reflect"
)

/* ---------------------------------------------------------------- *
 * METHOD array contains
 * ---------------------------------------------------------------- */

func ArrayContains(x interface{}, elem interface{}) bool {
	xAsArray := reflect.ValueOf(x)
	if xAsArray.Kind() == reflect.Slice {
		for i := 0; i < xAsArray.Len(); i++ {
			if xAsArray.Index(i).Interface() == elem {
				return true
			}
		}
	}
	return false
}

/* ---------------------------------------------------------------- *
 * METHOD sort
 * ---------------------------------------------------------------- */

func IsSortedListInt(X []int) bool {
	n := len(X)
	for i, x := range X {
		for j := i + 1; j < n; j++ {
			if X[j] < x {
				return false
			}
		}
	}
	return true
}

/* ---------------------------------------------------------------- *
 * METHOD insert, pop
 * ---------------------------------------------------------------- */

func PopIndexListInt(X []int, index int) []int {
	// NOTE: aus irgendeinem Grund reicht es nicht aus mit X zu arbeiten, denn sonst die u. s. Zeile überschreibt X
	X_ := make([]int, len(X))
	copy(X_, X)
	return append(X_[:index], X_[(index+1):]...)
}

/* ---------------------------------------------------------------- *
 * METHOD unique
 * ---------------------------------------------------------------- */

func UniqueListInt(X []int) []int {
	var ok bool
	m := map[int]bool{}
	X_unique := []int{}
	for _, x := range X {
		if _, ok = m[x]; !ok {
			X_unique = append(X_unique, x)
		}
	}
	return X_unique
}

func ContainsNoDuplicatesListInt(X []int) bool {
	return len(X) <= len(UniqueListInt(X))
}

/* ---------------------------------------------------------------- *
 * METHOD get value from array of unknown length
 * ---------------------------------------------------------------- */

func GetArrayStringValue(arr *[]string, index int, Default string) string {
	if arr != nil && len(*arr) > index {
		return (*arr)[index]
	}
	return Default
}

func GetArrayBoolValue(arr *[]bool, index int, Default bool) bool {
	if arr != nil && len(*arr) > index {
		return (*arr)[index]
	}
	return Default
}

func GetArrayIntValue(arr *[]int, index int, Default int) int {
	if arr != nil && len(*arr) > index {
		return (*arr)[index]
	}
	return Default
}

func GetArrayInterfaceValue(arr *[](interface{}), index int, Default interface{}) interface{} {
	if arr != nil && len(*arr) > index {
		return (*arr)[index]
	}
	return Default
}
