package metrics

/* ---------------------------------------------------------------- *
 * IMPORTS
 * ---------------------------------------------------------------- */

import (
	"fmt"
	"time"

	"ads/internal/core/utils"
	"ads/internal/types"
)

/* ---------------------------------------------------------------- *
 * GLOBAL VARIABLES/CONSTANTS
 * ---------------------------------------------------------------- */

var _ctr_time = types.NewCounter()
var _ctr_moves = types.NewCounter()
var _ctr_space = types.NewCounter()
var _tmr = types.NewTimer()

/* ---------------------------------------------------------------- *
 * METHODS
 * ---------------------------------------------------------------- */

func ResetMetrics() {
	_ctr_time.Reset()
	_ctr_moves.Reset()
	_ctr_space.Reset()
	_tmr.Reset()
}

func StartMetrics() {
	_tmr.Start()
}

func StopMetrics() {
	_tmr.Stop()
}

func AddTimeCost(options ...int) {
	_ctr_time.Add(options...)
}

func AddMovesCost(options ...int) {
	_ctr_moves.Add(options...)
}

func AddSpaceCost(options ...int) {
	_ctr_space.Add(options...)
}

func SetSpaceCost(n int) {
	_ctr_space.Set(n)
}

func GetTimeCost() int {
	return _ctr_time.Size()
}

func GetMovesCost() int {
	return _ctr_moves.Size()
}

func GetSpaceCost() int {
	return _ctr_space.Size()
}

func GetTimeElapsed() time.Duration {
	return _tmr.ElapsedTime()
}

func GetTimeElapsedLongFormat() string {
	t := _tmr.ElapsedTime()
	h := utils.Floor(t.Hours())
	m := utils.Floor(t.Minutes()) - h*60
	s := t.Seconds() - float64(m*60)
	h_string := fmt.Sprintf("%v", h)
	for len(h_string) < 2 {
		h_string = "0" + h_string
	}
	m_string := fmt.Sprintf("%v", m)
	for len(m_string) < 2 {
		m_string = "0" + m_string
	}
	s_string := fmt.Sprintf("%f", s)
	return fmt.Sprintf("%[1]s:%[2]s:%[3]s", h_string, m_string, s_string)
}
