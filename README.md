# Algorithmen und Datenstrukturen I, WiSe 2021-22 #

Diese Repository ist für die Seminargruppe **j/l** am Freitag um 13:15–14:45
im Raum SG 3-14 bzw. SG 4-10 (alternierend).

**HINWEIS 1:** In diesem Repository werden keine Personen bezogenen Daten der Studierenden gespeichert.

**HINWEIS 2:** Es besteht absolut gar keine Pflicht, die Materialien in diesem Repo zu verwenden. Diese sind lediglich zusätzliche Hilfsmittel. **Im Zweifelsfalls** sollte man sich immer an den Definitionen und Auslegungen in der VL orientieren.

In diesem Repository findet man:

- Protokolle der Seminargruppe [hier](./protocol).
- Notizen [hier](./notes).
- Symbolverzeichnis unter [notes/glossar.md](./notes/glossar.md).
- Referenzen unter [notes/quellen.md](./notes/quellen.md).

## Leistungen während des Semesters ##

- 12 x freiwillige Serien (unkorrigiert: Lösungen werden per Moodle veröffentlicht)
- 6 x Pflichtserien (jeweils 2 Wochen Bearbeitungsdauer)
- wöchentlich werden im den Lernstunden _andere_ Aufgabenblätter im Raum angezeigt und von Studierenden bearbeitet.

## Vorleistungen ##

- ≥ 50% der Punkte aus den 6 Pflichtserien sollen geschafft werden.
- _nur digitale Abgabe_!

## Code ##

In den Unterordnern
    [`code/golang`](./code/golang)
    und
    [`code/python`](./code/python)
kann man Codeprojekte finden.
Dort kann man konkrete Implementierung der Algorithmen ansehen
und ggf. auch auf echte Daten austesten.
